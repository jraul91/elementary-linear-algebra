{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import sympy as sy\n",
    "from PIL import Image\n",
    "\n",
    "import holoviews as hv; hv.extension('bokeh')\n",
    "import panel as pn; pn.extension()\n",
    "\n",
    "def pn_img( array, sz=200):\n",
    "    return pn.pane.Pane(Image.fromarray(array),width=sz)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\"><strong style=\"height:60px;color:darkred;font-size:40px;\">The Discrete Fourier Transform</strong></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some lectures that I like:\n",
    "\n",
    "|  $$\\text{Author} \\qquad\\qquad$$ | Link   |\n",
    "|:---- | :---- |\n",
    "| 3Blue1Brown | https://www.youtube.com/watch?v=spUNpyF58BY |\n",
    "| G. Strang | https://www.youtube.com/watch?v=M0Sa8fLOajA&list=PL49CF3715CB9EF31D&index=27 |\n",
    "| S. Brunton | https://www.youtube.com/watch?v=nl9TZanwbBk |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 1. The Discrete Fourier Basis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def define_w(n):\n",
    "    '''n^th root of 1'''\n",
    "    return sy.exp(2j*sy.pi/n)\n",
    "\n",
    "def FourierMatrix(n):\n",
    "    '''Entry F_ij = w ^ (i j)'''\n",
    "    w = define_w(n)\n",
    "    l = sy.Matrix( [[ (w**i)**j for j in range(n)] for i in range(n)] )\n",
    "    return l\n",
    "\n",
    "def numFourierMatrix(n):\n",
    "    # compute the numerical representation with the 1/sqrt(n) scaling included\n",
    "    w = np.cos(2*np.pi/n)+1j*np.sin(2*np.pi/n)  # convert to floating point\n",
    "    l = np.array( [[ w**(p*k) for k in range(n)] for p in range(n)] )*1/np.sqrt(n)\n",
    "    return l\n",
    "\n",
    "F = FourierMatrix(4)   # multiply by 1/sqrt(n) to make the matrix unitary. See below\n",
    "F"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:65%;\">\n",
    "The Fourier Matrix $F$ of size $n \\times n$ is an invertible matrix defined<br>$\\quad$  from a <strong>root of unity</strong> $w = e^{i \\frac{2 \\pi}{n}}$:<br>\n",
    "$\\qquad \\boxed{F = w^{i j}}\\;\\; i=0,1,\\dots (n-1),\\;\\; j=0,1,\\dots (n-1).$\n",
    "</div>\n",
    "<div style=\"float:left;width:25%;padding-left:1cm;\">\n",
    "<img src=\"Figs/3rd_roots_of_unity.svg\" width=100>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "FFh = F*F.T.conjugate()                  # F times its Hermitian Transpose\n",
    "FFh.applyfunc(lambda x: sy.N(x,3))\\\n",
    "   .applyfunc( lambda x: 0 if abs(x) < 1e-10 else x ) # no surprise: we just need to scale M or its hermitian transpose\n",
    "                                         # by convention, we scale both F and F^h by 1/sqrt(N)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Fourier Matrix $F$ is invertible. since $F F^H = N$, the size of the matrix, the inverse of $F$ is given by<br>\n",
    "$\\quad F^{-1} = \\frac{1}{N} F^H$.\n",
    "\n",
    "A good choice of a scale factor for $F$ is to redefine the Fourier matrix by scaling it by $\\frac{1}{\\sqrt{N}}$. Then<br>\n",
    "$\\quad F \\leftarrow \\frac{1}{\\sqrt{N}} F$, so that $F^{-1} = F^H$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 2. Example: Sines Sampled at N=256 Values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 2.1 Example 1: A Sine Function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's set up a function of time sampled at $N = 256$ points"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N  = 256\n",
    "F  = numFourierMatrix(N)\n",
    "Fh = F.T.conj()\n",
    "\n",
    "# ------------------------------------------------------------\n",
    "t = np.linspace(0, 1, N)\n",
    "x = 2*np.sin(2*np.pi*10*t)   # period of the sine is f=1/10 secs\n",
    "\n",
    "pn.Row(hv.Curve((t,x), \"t\", \"x\")\\\n",
    "         .opts(title=\"Function  y = 2 sin( 2pi f t)\", height=200, width=500) *\\\n",
    "       hv.Scatter((t,x))\n",
    "      )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Change of Basis**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we think of $x$ as a vector, then $y = F x\\;$ rewrites $x$<br>$\\quad$ as a linear combination of the columns of $F^{-1}:\\quad x = F^{-1} y$\n",
    "\n",
    "$\\quad$ This is just a **change of basis!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "##### **The Basis Vectors**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What are the basis vectors? Since the columns of $F$ are complex,<br>\n",
    "$\\quad$ we will **plot both the real and imaginary parts of each column**<br>\n",
    "$\\quad$ versus the index of the column entry:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot( N, basis_vector_index, both=False ):\n",
    "    h = hv.Curve(np.real(Fh[:,basis_vector_index]), \"t\", \"x\",   label='real part')*\\\n",
    "        hv.Curve(np.imag(Fh[:,basis_vector_index]),   label='imag part')\n",
    "\n",
    "    if both and (basis_vector_index != 0): # index 0 would just repeat...\n",
    "        h = h * \\\n",
    "        hv.Curve(np.real(Fh[:,N-basis_vector_index]), label='real part(N-i)')*\\\n",
    "        hv.Curve(np.imag(Fh[:,N-basis_vector_index]), label='imag part(N-i)')\n",
    "        title = f'Basis Vectors # {basis_vector_index} and {N-basis_vector_index}'\n",
    "    else:\n",
    "        title=f'Basis Vector # {basis_vector_index}'\n",
    "\n",
    "    return h.opts( title=title, width=650, legend_position='right')\\\n",
    "            .opts(\"Curve\", muted_alpha=0)\n",
    "pn.Row(pn.interact( lambda both, basis_vector_index: plot(N, basis_vector_index, both),\n",
    "             basis_vector_index=(0,N//2), both=pn.widgets.Checkbox(name=\" show both i and N-i components\")))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the basis vectors are sines and cosines with increasing frequencies!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Alternate view: real and imaginary parts (hard to interpret...)\n",
    "#    hierarchic and highly symmetric multiscale structure to F.\n",
    "#hv.Image(np.real(F)).opts(cmap='gray')+ hv.Image(np.imag(F)).opts(cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "##### **The Coordinate Vector of the Function**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The x vector we computed above was one such sine.<br>\n",
    "$\\quad$ Let's compute the corresponding y vector"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The entries in this vectors are the complex multipliers of the columns of $F$.<br>\n",
    "$\\quad$ We are interested in their magnitude"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = np.arange(N)  # the index of the basis vector\n",
    "y = F @ x\n",
    "pn.Row(hv.Spikes((n,abs(y)/np.sqrt(N)),\"n\", \"y\", label=\"magnitude of the component vector y\").opts(width=650))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reason we got two peaks is that we used a real vector x,<br>\n",
    "$\\quad$ i.e., not really a basis vector, but the difference of two basis vectors:<br>\n",
    "$\\quad$ the columns of $F$ at index 10, and index 256-10 (counting from 0)\n",
    "\n",
    "Note there is some bleeding of the actual component to neighboring basis vectors<br>\n",
    "$\\quad$ due to inexact numerical computations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:** drawing these plots as curves is misleading: we have a **discrete set of values**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 2.2 Example 2: Sine and a Cosine, Different Frequencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# What if we make the function more complicated?\n",
    "x = 2.0*np.sin(2*np.pi*10*t) \\\n",
    "   +0.5*np.cos(2*np.pi*100*t)\n",
    "y = F @ x\n",
    "pn.Row( hv.Spikes((t,x), \"t\", \"x\", label='x').opts(show_legend=False)*\\\n",
    "        hv.Curve( (t,x), \"t\", \"x\", label='x').opts(show_legend=False, line_width=0.8),\n",
    "        hv.Spikes((n,abs(y)/np.sqrt(N)),\"index\", \"abs_y\", label=\"|y|\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we created an x vector composed of a sine with a superimposed wiggle with frequencies 10 and 100 Hz,<br>\n",
    "and therefore, there are 4 entries in the transformed coefficient vector:<br> entries at 10 and 100, as well as 256-10, 256-100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 3. A Function of Time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 3.1 Sample a Function $x(t)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# What if we make the function more complicated?\n",
    "x = 2.0*(0.5-t)**3 + 0.1*t \\\n",
    "   + 0.01*np.cos(2*np.pi*100*t)  # add a small, fast wiggle to the cubic\n",
    "y = F @ x\n",
    "pn.Row( hv.Curve( (t,x), \"t\", \"x\", label='x').opts(show_legend=False),\n",
    "        hv.Spikes((n,abs(y)/np.sqrt(N)),\"index\", \"abs_y\", label=\"|y|\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a lot of small entries In addition to the peaks at n=100, 256-n for the wiggle!<br>\n",
    "This is easier to see on a log scale "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_abs = abs(y)/np.sqrt(N)\n",
    "y_min =  min(y_abs)\n",
    "y_max =  max(y_abs)\n",
    "print(\"Smallest coefficient is\", y_min )\n",
    "pn.Row((hv.Curve((n,y_abs), \"index\", \"abs_y\")\\\n",
    "         .opts(logy=True,ylim=(y_min,1.01*y_max), width=600,muted_alpha=0,yticks=5,line_width=0.5,\n",
    "               title = \"coefficient vector abs.(y)\")*\\\n",
    "        hv.Spikes((n,y_abs-y_min), \"index\", \"abs_y\").opts(logy=True,ylim=(y_min,1.01*y_max), position=y_min))\n",
    "      )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What if we **throw out some of those small values,** (they are just small wiggles),<br>\n",
    "and transform back\n",
    "to our original basis? Is $\\tilde{x} \\approx F y$?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 3.2 Removing Some Fourier Coefficients"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ditch_values( y, smallest ):\n",
    "    y_approx = np.array( [ v if abs(v) > smallest else 0 for v in y] )\n",
    "    return y_approx\n",
    "\n",
    "def ditch_values_plot( y, smallest ):\n",
    "    y_approx = ditch_values( y, smallest )\n",
    "    h        = hv.Spikes( (n,abs(y_approx)), \"index\", \"abs_y\", label='larger values only' ) *\\\n",
    "               hv.Curve( (n, abs(y)),        label='all values' ).opts(line_width=0.5)\n",
    "    return h.opts( legend_position = 'right', width=700)\n",
    "\n",
    "pn.Row(ditch_values_plot(y, 0.05))   # 0.05 still includes the fast wiggle!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def recovered_plot(y,  smallest ):\n",
    "    y_approx = ditch_values( y, smallest )\n",
    "    x_approx = Fh @ y_approx\n",
    "\n",
    "    h_error = hv.Curve( (t, np.abs(x_approx - x)), \"t\", \"x - x_recovered\" ).opts(title=\"Error\")\n",
    "    h       = hv.Curve( (t,np.real(x)), \"t\", \"x\", label='original').opts( width=500, title=\"Original and Recovered Curve\" ) *\\\n",
    "              hv.Curve( (t,np.real(x_approx)), \"t\", \"x\", label='approx')\n",
    "    return h_error + h.opts(legend_position=\"right\")\n",
    "    \n",
    "pn.Row( recovered_plot(y, 2e-3 ) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pn.interact( lambda smallest: recovered_plot( y, smallest).opts(title=\"Remove smallest Fourier Coefficients\"),\n",
    "             smallest=pn.widgets.FloatSlider(name='smallest',\n",
    "                        start=y_min, end=y_min+0.2*(y_max-y_min), step=0.2*(y_max-y_min)/100) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Click on the magnifying glass to zoom\n",
    "\n",
    "**Remark:** The expansion forces the extreme y values to be the same; hence the large errors near the endpoints of the time interval"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Generalization to Higher Dimensional Spaces"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For an image, we simply take the Fourier Transform of each row, <br>\n",
    "$\\quad$ followed by the Fourier transform of each column of the intermediate output.\n",
    "\n",
    "The result does not depend on which transform is used first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img = Image.open( 'Figs/cat.png' )\n",
    "cat = np.array(img.convert(mode='L'))\n",
    "\n",
    "def fft_2D(A):\n",
    "    '''the corresponding library routine is C = np.fft.fft2(A)'''\n",
    "    B = np.zeros_like(A,dtype='complex')\n",
    "    for j in range(B.shape[0]):\n",
    "        B[j,:] = np.fft.fft(A[j,:])\n",
    "    C = np.zeros_like(B)\n",
    "    for j in range(C.shape[1]):\n",
    "        C[:,j] = np.fft.fft(B[:,j])\n",
    "    return B, C\n",
    "\n",
    "cat_rowfft, cat_fft = fft_2D( cat )\n",
    "\n",
    "h = \\\n",
    "pn.Row(hv.Image(cat).opts(xaxis=None,yaxis=None, cmap='gray', title='Original'),\n",
    "       hv.Image( np.fft.fftshift(np.log(np.abs(cat_rowfft))) ).opts(xaxis=None,yaxis=None, cmap='gray', title='Intermediate (DFT of Rows)'),\n",
    "       hv.Image( np.fft.fftshift(np.log(np.abs(cat_fft   ))) ).opts(xaxis=None,yaxis=None, cmap='gray', title='2D DFT')\n",
    "      )\n",
    "h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 5. Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The change of basis to Fourier Coefficients **reveals features** of the original series<br>\n",
    "$\\quad$ that were not readily apparent."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can analyze the function in either representation, and introduce transformations<br>\n",
    "$\\quad$ in one domain that may be much harder in the other.\n",
    "\n",
    "Note the clipping function *ditch_values()* we applied is **non-linear**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The structure of the $F$ matrix can be exploited to greatly **speed up the computation** of $F x$<br>\n",
    "$\\quad$ (the **Fast Fourier Transform**)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
