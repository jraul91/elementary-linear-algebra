{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "using LinearAlgebra, RowEchelon, LaTeXStrings, Plots\n",
    "using SymPy, Latexify\n",
    "#using LAcode\n",
    "#include(\"LAcodes.jl\")\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\"><strong style=\"height:60px;color:darkred;font-size:40px;\">Linear Transformations: 3D Examples</strong><br><strong style=\"height:45px;color:darkred;font-size:25px;\">Matrices Computed Using the Normal Equations</strong></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 1. A Plane through the Origin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 1.1 Orthogonal Projection Onto a Plane $n \\cdot x = 0$ and Onto a Normal to the Plane"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the equation of a plane through the origin: $n_1 x_1 + n_2 x_2 + n_3 x_3 = 0$.<br>\n",
    "$\\quad$ Without loss of generality, assume $n_1 \\ne 0$.\n",
    "\n",
    "Viewed as a system of linear equations, we have $A x = 0$ with $A = \\begin{pmatrix} n_1 & n_2 & n_3 \\end{pmatrix},$ and $x = \\begin{pmatrix} x_1 \\\\ x_2 \\\\ x_3 \\end{pmatrix}.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The rowspace $\\mathscr{R}(A)$ has basis $\\left\\{\\; n = \\begin{pmatrix} n_1 \\\\ n_2 \\\\ n_3 \\end{pmatrix}\\;\\; \\right\\}\\quad$ (the gradient for the plane).<br>\n",
    "* The null space $\\mathscr{N}(A)$ has a basis $\\left\\{\\; v_1=\\begin{pmatrix} -n_2 \\\\ n_1 \\\\ 0 \\end{pmatrix},\\;  v_2=\\begin{pmatrix} -n_3 \\\\ 0 \\\\ n_1 \\end{pmatrix}\\;\\; \\right\\}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "<div style=\"float:left;padding-right:1cm;width:45%;\">\n",
    "The orthogonal projection matrix into the row space is given by\n",
    "$$P_\\perp = \\frac{1}{n\\cdot n} n n^t = \\frac{1}{n \\cdot n} \\begin{pmatrix} n^2_1 & n_1 n_2 & n_1 n_3 \\\\ n_2 n_1 & n^2_2 & n_2 n_3 \\\\ n_3 n_1 & n_3 n_2 & n^2_3\\end{pmatrix}$$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:1cm;width:45%;border-left:2px solid black;\">\n",
    "The orthogonal projection matrix onto the null space (i.e. the plane) is given by\n",
    "$$P_\\parallel = I - P_\\perp = \\frac{1}{n \\cdot n} \\begin{pmatrix} n^2_2+n^2_2 & -n_1 n_2 & -n_1 n_3 \\\\ -n_2 n_1 & n^2_1+n^2_3 & -n_2 n_3 \\\\ -n_3 n_1 & -n_3 n_2 & n^2_1+n^2_2\\end{pmatrix}$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"width:80%;\">\n",
    "\n",
    "> **Example:**<br>\n",
    "> The orthogonal projection matrices defined for the plane $x+y+z=0$ are given by<br><br>\n",
    "$$\n",
    "    P_\\perp     =  \\frac{1}{3} \\begin{pmatrix} 1 & 1 & 1 \\\\ 1 & 1 & 1 \\\\ 1 & 1 & 1 \\end{pmatrix}, \\qquad\\qquad\n",
    "    P_\\parallel =  \\frac{1}{3} \\begin{pmatrix} 2 & -1 & -1 \\\\ -1 & 2 & -1 \\\\ -1 & -1 & 2 \\end{pmatrix}\n",
    "$$\n",
    "    </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 1.2 The Mirror Image of a Point With Respect to the Plane $n \\cdot x = 0$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-right:1cm;padding-top:0.5cm;width:60%;\">\n",
    "We obtain the reflection of a point $A$ through the plane<br><br>\n",
    "\n",
    "$\\quad OB = OA - 2 \\left( OA - OC \\right),$ i.e.,<br><br>\n",
    "$\\quad \\begin{align}\\tilde{x} =&\\ x - 2 P_\\perp x \\\\ =&\\ (I - 2 P_\\perp) x \\end{align}$\n",
    "\n",
    "$(L)$ is the line from the origin<br>$\\quad$ through the orthogonal projection point $C$ in the plane.\n",
    "</div>\n",
    "<div style=\"float:left;width:30%;border:2px solid black;padding-top:1cm;\">\n",
    "<img src=\"./Figs/LinTxSymLine.svg\"  width=\"180\">\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reflection matrix is given by\n",
    "$$\n",
    "R = I - 2 P_\\perp = \\frac{1}{n \\cdot n} \\begin{pmatrix} -n^2_1+n^2_2+n^2_3 & -2 n_1 n_2 & -2 n_1 n_3 \\\\ -2 n_2 n_1 & n^2_1- n^2_2+n^2_3 & -2 n_2 n_3 \\\\ -2 n_3 n_1 & -2 n_3 n_2 & n^2_1+n^2_2-n^2_3\\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"width:70%;\">\n",
    "\n",
    "> **Example:**<br>\n",
    "> the reflection matrix defined for the plane $x+y+z=0$ is given by<br><br>\n",
    "$$\n",
    "R = \\frac{1}{3} \\begin{pmatrix} 1 & -2 & -2 \\\\ -2 & 1 & -2 \\\\ -2 & -2 & 1 \\end{pmatrix}\n",
    "$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true",
    "tags": []
   },
   "source": [
    "#### **Implementation**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "x1,x2,x3 = symbols(\"x_1,x_2,x_3\", real=true); x = [x1;x2;x3]\n",
    "d1,d2,d3 = symbols(\"d_1,d_2,d_3\", real=true); d = [d1;d2;d3]\n",
    "n1,n2,n3 = symbols(\"n_1,n_2,n_3\", real=true); n = [n1;n2;n3]\n",
    "b        = symbols(\"b\",           real=true)\n",
    ";"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Projection P_⟂ ="
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{rrr}\\frac{n_{1}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\\\frac{n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{2}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\\\frac{n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{3}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\end{array}\\right]$\n"
      ],
      "text/plain": [
       "3×3 Array{Sym,2}:\n",
       "   n_1^2/(n_1^2 + n_2^2 + n_3^2)  …  n_1*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       " n_1*n_2/(n_1^2 + n_2^2 + n_3^2)     n_2*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       " n_1*n_3/(n_1^2 + n_2^2 + n_3^2)       n_3^2/(n_1^2 + n_2^2 + n_3^2)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "P_perp     = 1/dot(n,n) * n*n'\n",
    "P_parallel = simplify.(diagm(ones(Sym, 3)) - P_perp)   # I - P_perp\n",
    "R          = simplify.(diagm(ones(Sym, 3)) - 2*P_perp)\n",
    "\n",
    "print(\"Projection P_⟂ =\")\n",
    "P_perp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Projection P_∥ ="
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{rrr}\\frac{n_{2}^{2} + n_{3}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&- \\frac{n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&- \\frac{n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\- \\frac{n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{1}^{2} + n_{3}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&- \\frac{n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\- \\frac{n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&- \\frac{n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{1}^{2} + n_{2}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\end{array}\\right]$\n"
      ],
      "text/plain": [
       "3×3 Array{Sym,2}:\n",
       " (n_2^2 + n_3^2)/(n_1^2 + n_2^2 + n_3^2)  …         -n_1*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       "        -n_1*n_2/(n_1^2 + n_2^2 + n_3^2)            -n_2*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       "        -n_1*n_3/(n_1^2 + n_2^2 + n_3^2)     (n_1^2 + n_2^2)/(n_1^2 + n_2^2 + n_3^2)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "print(\"Projection P_∥ =\")\n",
    "P_parallel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Reflection R="
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{rrr}\\frac{- n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&- \\frac{2 n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&- \\frac{2 n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\- \\frac{2 n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{1}^{2} - n_{2}^{2} + n_{3}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&- \\frac{2 n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\- \\frac{2 n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&- \\frac{2 n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{1}^{2} + n_{2}^{2} - n_{3}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\end{array}\\right]$\n"
      ],
      "text/plain": [
       "3×3 Array{Sym,2}:\n",
       " (-n_1^2 + n_2^2 + n_3^2)/(n_1^2 + n_2^2 + n_3^2)  …               -2*n_1*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       "               -2*n_1*n_2/(n_1^2 + n_2^2 + n_3^2)                  -2*n_2*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       "               -2*n_1*n_3/(n_1^2 + n_2^2 + n_3^2)     (n_1^2 + n_2^2 - n_3^2)/(n_1^2 + n_2^2 + n_3^2)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "print(\"Reflection R=\")\n",
    "R"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. A Plane Not Containing the Origin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 2.1 Orthogonal Projection and Reflection Through a Plane $n \\cdot x = b$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A plane that does not contain the origin **does not** define a subspace of $\\mathbb{R}^3.$\n",
    "\n",
    "To use a linear transformation, we must first **translate the origin of the coordinate system** to lie in this plane, e.g.,<br><br>\n",
    "$$\\left.\n",
    "\\begin{align}\n",
    "& n_1 x_1 + n_2 x_2 + n_3 x_3 = b, \\quad \\text{ with } \\quad b \\ne 0, n_1 \\ne 0 \\\\\n",
    "& \\tilde{x} = T x = x + d, \\text{ where } d = \\begin{pmatrix} - \\frac{b}{n_1} \\\\ 0\\\\0 \\end{pmatrix}\n",
    "\\end{align}\\quad \\right\\} \\Rightarrow \\quad n_1 \\tilde{x}_1 + n_2 \\tilde{x}_2 + n_3 \\tilde{x}_3 = 0.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then use the matrices derived above to compute the projections with respect to the $\\tilde{x}$ coordinate system,<br>\n",
    "$\\quad$ and use $T^{-1}$ to translate these back into the original coordinate system:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "<div style=\"height:6cm;\">\n",
    "<div style=\"float:left;padding-left:0cm;padding-right:0.3cm;width:30%;\">\n",
    "Orthogonal projection into the plane:<br><br>\n",
    "$\\quad \\boxed{x \\xrightarrow{T} \\tilde{x} \\xrightarrow{P_\\parallel} \\tilde{x}_\\parallel \\xrightarrow{T^{-1}} x_\\parallel}$\n",
    "<br><br>\n",
    "$\\begin{align}\n",
    "x_\\parallel =&\\  T^{-1} P_\\parallel T x \\\\\n",
    "=&\\ (P_\\parallel (x+d)) -d \\\\\n",
    "=&\\ P_\\parallel x  + (P_\\parallel -I) d \\\\\n",
    "=&\\ P_\\parallel x - P_\\perp d\n",
    "\\end{align}\n",
    "$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:0.5cm;width:30%;border-left:2px solid black;border-right:2px solid black;\">\n",
    "Orthogonal Projection onto the normal to the plane:<br><br>\n",
    "$\\quad \\boxed{x \\xrightarrow{T} \\tilde{x} \\xrightarrow{P_\\perp} \\tilde{x}_\\perp \\xrightarrow{T^{-1}} x_\\perp}$\n",
    "<br><br>\n",
    "$\\begin{align}\n",
    "x_\\perp =&\\  T^{-1} P_\\parallel T x \\\\\n",
    "=&\\ (P_\\perp (x+d)) -d \\\\\n",
    "=&\\ P_\\perp x  + (P_\\perp -I) d \\\\\n",
    "=&\\ P_\\perp x - P_\\parallel d\n",
    "\\end{align}\n",
    "$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:0.5cm;width:30%;\">\n",
    "Reflection through the plane:<br><br>\n",
    "$\\quad \\boxed{x \\xrightarrow{T} \\tilde{x} \\xrightarrow{R} \\tilde{x}_{mirror} \\xrightarrow{T^{-1}} x_{mirror}}$\n",
    "<br><br>\n",
    "$\\begin{align}\n",
    "x_{mirror} =&\\  T^{-1} R T x \\\\\n",
    " =&\\ (R (x+d)) -d \\\\\n",
    " =&\\ R x  - 2 P_\\perp d \\\\\n",
    "\\end{align}\n",
    "$<br>\n",
    "$\\ $where $\\;\\; R = I - 2 P_\\perp $\n",
    "</div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true",
    "tags": []
   },
   "source": [
    "#### **Implementation**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{r}\\frac{d_{1} \\left(n_{2}^{2} + n_{3}^{2}\\right)}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} - \\frac{d_{2} n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} - \\frac{d_{3} n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\- \\frac{d_{1} n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} + \\frac{d_{2} \\left(n_{1}^{2} + n_{3}^{2}\\right)}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} - \\frac{d_{3} n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\- \\frac{d_{1} n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} - \\frac{d_{2} n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} + \\frac{d_{3} \\left(n_{1}^{2} + n_{2}^{2}\\right)}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\end{array} \\right]$\n"
      ],
      "text/plain": [
       "3-element Array{Sym,1}:\n",
       "  d_1*(n_2^2 + n_3^2)/(n_1^2 + n_2^2 + n_3^2) - d_2*n_1*n_2/(n_1^2 + n_2^2 + n_3^2) - d_3*n_1*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       " -d_1*n_1*n_2/(n_1^2 + n_2^2 + n_3^2) + d_2*(n_1^2 + n_3^2)/(n_1^2 + n_2^2 + n_3^2) - d_3*n_2*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       " -d_1*n_1*n_3/(n_1^2 + n_2^2 + n_3^2) - d_2*n_2*n_3/(n_1^2 + n_2^2 + n_3^2) + d_3*(n_1^2 + n_2^2)/(n_1^2 + n_2^2 + n_3^2)"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d = [d1;d2;d3]\n",
    "x = [x1;x2;x3]\n",
    "\n",
    "p_perp_d     = P_perp*d\n",
    "p_parallel_d = P_parallel*d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the plane $x + y + z = 1$, we can choose $d = \\begin{pmatrix} -1 \\\\ 0 \\\\ 0 \\end{pmatrix}$ to obtain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Orthogonal Projection into the plane\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{r}\\frac{2 x_{1}}{3} - \\frac{x_{2}}{3} - \\frac{x_{3}}{3} + \\frac{1}{3}\\\\- \\frac{x_{1}}{3} + \\frac{2 x_{2}}{3} - \\frac{x_{3}}{3} + \\frac{1}{3}\\\\- \\frac{x_{1}}{3} - \\frac{x_{2}}{3} + \\frac{2 x_{3}}{3} + \\frac{1}{3}\\end{array}\\right]$\n"
      ],
      "text/plain": [
       "3×1 Array{Sym,2}:\n",
       "  2*x_1/3 - x_2/3 - x_3/3 + 1/3\n",
       " -x_1/3 + 2*x_2/3 - x_3/3 + 1/3\n",
       " -x_1/3 - x_2/3 + 2*x_3/3 + 1/3"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "println(\"Orthogonal Projection into the plane\")\n",
    "x_parallel = P_parallel * x - p_perp_d\n",
    "x_parallel.subs( Dict( d1=>-1, d2=>0, d3=>0, n1=>1, n2=>1, n3=> 1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Orthogonal Projection onto the normal to the plane\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{r}\\frac{x_{1}}{3} + \\frac{x_{2}}{3} + \\frac{x_{3}}{3} + \\frac{2}{3}\\\\\\frac{x_{1}}{3} + \\frac{x_{2}}{3} + \\frac{x_{3}}{3} - \\frac{1}{3}\\\\\\frac{x_{1}}{3} + \\frac{x_{2}}{3} + \\frac{x_{3}}{3} - \\frac{1}{3}\\end{array}\\right]$\n"
      ],
      "text/plain": [
       "3×1 Array{Sym,2}:\n",
       " x_1/3 + x_2/3 + x_3/3 + 2/3\n",
       " x_1/3 + x_2/3 + x_3/3 - 1/3\n",
       " x_1/3 + x_2/3 + x_3/3 - 1/3"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "println(\"Orthogonal Projection onto the normal to the plane\")\n",
    "x_perp = P_perp*x - p_parallel_d\n",
    "x_perp.subs( Dict( d1=>-1, d2=>0, d3=>0, n1=>1, n2=>1, n3=> 1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Reflection through the plane\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{r}\\frac{x_{1}}{3} - \\frac{2 x_{2}}{3} - \\frac{2 x_{3}}{3} + \\frac{2}{3}\\\\- \\frac{2 x_{1}}{3} + \\frac{x_{2}}{3} - \\frac{2 x_{3}}{3} + \\frac{2}{3}\\\\- \\frac{2 x_{1}}{3} - \\frac{2 x_{2}}{3} + \\frac{x_{3}}{3} + \\frac{2}{3}\\end{array}\\right]$\n"
      ],
      "text/plain": [
       "3×1 Array{Sym,2}:\n",
       "  x_1/3 - 2*x_2/3 - 2*x_3/3 + 2/3\n",
       " -2*x_1/3 + x_2/3 - 2*x_3/3 + 2/3\n",
       " -2*x_1/3 - 2*x_2/3 + x_3/3 + 2/3"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "println(\"Reflection through the plane\")\n",
    "x_mirror = R*x - 2*p_perp_d\n",
    "x_mirror.subs( Dict( d1=>-1, d2=>0, d3=>0, n1=>1, n2=>1, n3=> 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### **Implementation Using Homogeneous Coordinates**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "P_⟂ in Homogeneous Coordinates"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{rrrr}\\frac{1}{3}&\\frac{1}{3}&\\frac{1}{3}&0\\\\\\frac{1}{3}&\\frac{1}{3}&\\frac{1}{3}&0\\\\\\frac{1}{3}&\\frac{1}{3}&\\frac{1}{3}&0\\\\0&0&0&1\\end{array}\\right]$\n"
      ],
      "text/plain": [
       "4×4 Array{Sym,2}:\n",
       " 1/3  1/3  1/3  0\n",
       " 1/3  1/3  1/3  0\n",
       " 1/3  1/3  1/3  0\n",
       "   0    0    0  1"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function embed(M)\n",
    "    zeros = [0;0;0]\n",
    "    [ M      zeros \n",
    "     zeros'     1 ]\n",
    "end\n",
    "function embed(v :: Array{Sym,1})\n",
    "    [v;1]\n",
    "end\n",
    "print(\"P_⟂ in Homogeneous Coordinates\")\n",
    "embed( P_perp.subs( Dict( n1=>1, n2=>1, n3=> 1)) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "function T()\n",
    "    [1 0 0 d1\n",
    "     0 1 0 d2\n",
    "     0 0 1 d3\n",
    "     0 0 0 1]\n",
    "end\n",
    "function Tinverse()\n",
    "    [1 0 0 -d1\n",
    "     0 1 0 -d2\n",
    "     0 0 1 -d3\n",
    "     0 0 0  1]\n",
    "end\n",
    ";"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{rrrr}\\frac{n_{1}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{d_{1} n_{1}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} - d_{1} + \\frac{d_{2} n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} + \\frac{d_{3} n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\\\frac{n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{2}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{d_{1} n_{1} n_{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} + \\frac{d_{2} n_{2}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} - d_{2} + \\frac{d_{3} n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}\\\\\\frac{n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{n_{3}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}}&\\frac{d_{1} n_{1} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} + \\frac{d_{2} n_{2} n_{3}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} + \\frac{d_{3} n_{3}^{2}}{n_{1}^{2} + n_{2}^{2} + n_{3}^{2}} - d_{3}\\\\0&0&0&1\\end{array}\\right]$\n"
      ],
      "text/plain": [
       "4×4 Array{Sym,2}:\n",
       "   n_1^2/(n_1^2 + n_2^2 + n_3^2)  …  d_1*n_1^2/(n_1^2 + n_2^2 + n_3^2) - d_1 + d_2*n_1*n_2/(n_1^2 + n_2^2 + n_3^2) + d_3*n_1*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       " n_1*n_2/(n_1^2 + n_2^2 + n_3^2)     d_1*n_1*n_2/(n_1^2 + n_2^2 + n_3^2) + d_2*n_2^2/(n_1^2 + n_2^2 + n_3^2) - d_2 + d_3*n_2*n_3/(n_1^2 + n_2^2 + n_3^2)\n",
       " n_1*n_3/(n_1^2 + n_2^2 + n_3^2)     d_1*n_1*n_3/(n_1^2 + n_2^2 + n_3^2) + d_2*n_2*n_3/(n_1^2 + n_2^2 + n_3^2) + d_3*n_3^2/(n_1^2 + n_2^2 + n_3^2) - d_3\n",
       "                               0                                                                                                                       1"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "PerpProj = Tinverse()*embed(P_perp) * T()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\left[ \\begin{array}{r}\\frac{x_{1}}{3} + \\frac{x_{2}}{3} + \\frac{x_{3}}{3} + \\frac{2}{3}\\\\\\frac{x_{1}}{3} + \\frac{x_{2}}{3} + \\frac{x_{3}}{3} - \\frac{1}{3}\\\\\\frac{x_{1}}{3} + \\frac{x_{2}}{3} + \\frac{x_{3}}{3} - \\frac{1}{3}\\\\1\\end{array}\\right]$\n"
      ],
      "text/plain": [
       "4×1 Array{Sym,2}:\n",
       " x_1/3 + x_2/3 + x_3/3 + 2/3\n",
       " x_1/3 + x_2/3 + x_3/3 - 1/3\n",
       " x_1/3 + x_2/3 + x_3/3 - 1/3\n",
       "                           1"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x_perp  = PerpProj * embed(x)\n",
    "x_perp.subs( Dict( d1=>-1, d2=>0, d3=>0, n1=>1, n2=>1, n3=> 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.4",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
