{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "heading_collapsed": "false",
    "tags": []
   },
   "outputs": [],
   "source": [
    "using LinearAlgebra, RowEchelon, LaTeXStrings, Plots, SymPy, LAcode\n",
    "##] dev --local \".\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\"><strong style=\"height:100px;color:darkred;font-size:40px;\">The Gram Matrix Eigenproblem</strong></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 1. The Gram Matrix $A^t A$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 1.1 Rank and the Dimension of the Null Spaces"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "Some reminders for matrices $A$ of size $M \\times N$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "<div style=\"float:left;width:13cm;height:4cm;border:1px solid black;\">\n",
    "\n",
    "$\\;\\;$ We have previously seen that $\\mathscr{N}(A^t A)\\ =\\ \\mathscr{N}(A).$<br>\n",
    "$\\;\\;$ Therefore\n",
    "* $dim\\ \\mathscr{N}(A^t A)\\ =\\ dim\\ \\mathscr{N}(A) = N -rank(A)$\n",
    "* $rank\\ (A) = rank\\ (A^t A)$<br>since both matrices have the same number of columns $N$.\n",
    "</div>\n",
    "<div style=\"float:right;width:12cm;height:4cm;border:1px solid black;\">\n",
    "\n",
    "$\\;\\;$ Similarly, $\\mathscr{N}(A A^t)\\ =\\ \\mathscr{N}(A^t).$<br>\n",
    "$\\;\\;$ Therefore\n",
    "* $dim\\ \\mathscr{N}(A A^t)\\ =\\ dim\\mathscr{N}(A^t)\\ =\\ M - rank\\ (A^t)$\n",
    "* $rank\\ (A^t)\\ =\\ rank\\ (A A^t)$<br>since both matrices have the same number of columns $M$.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**The matrices $A$, $A^t$, $A^t A$ and $A A^t$ have the same rank.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 1.2 Dimension of the Eigenspaces for $\\lambda = 0$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "<div style=\"float:left;width:12cm;height:5.5cm;border:1px solid black;\">\n",
    "$\\;\\;$ We also know that $rank(A) = rank(A^t)\\;$<br>\n",
    "$\\;\\;\\quad$ and that non-zero nullspace vectors are eigenvectors for $\\lambda =0$.\n",
    "\n",
    "* $A, A^t, A^t A$ and $A A^t$ all have the same rank.\n",
    "* Eigenspace $E_0$ of $A^t A$ has $dim\\ \\mathscr{N}(A^t A) = N - rank(A)$\n",
    "* Eigenspace $E_0$ of $A A^t$ has $dim\\ \\mathscr{N}(A^t A) = M - rank(A)$\n",
    "* $A^t A$ and $A A^t$ have the same number<br>$\\quad$ of **non-zero eigenvalues:** $rank(A)$ \n",
    "</div>\n",
    "<img src=\"Figs/SVD_ranks.svg\" width=300 style=\"float:left;padding-left:5cm;\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 1.3 Relationship of Eigenpairs for $A^t A$ and $A A^t$ for Non-zero Eigenvalues"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "<div style=\"float:left;width:14.5cm;height:4.5cm;border:1px solid black;\">\n",
    "\n",
    "$\\;\\;$ Let $(\\lambda, x)$ be an eigenpair of $A^t A$. Observe\n",
    "$$\n",
    "(A^t A)\\ x = \\lambda x \\Rightarrow (A A^t) (A x) = \\lambda (A x)\n",
    "$$\n",
    "\n",
    "$\\;\\;\\therefore$ If $A x \\ne 0,$ it is an eigenvector of $A A^t.$ Is it?<br><br>\n",
    "\n",
    "$\\;\\;$ **$(\\lambda \\ne 0, x)$ is an eigenpair of $A^t A \\Rightarrow (\\lambda, A x)$ is an eigenpair of $A A^t.$**<br>\n",
    "$\\;\\;$ **$(\\lambda \\ne 0, x)$ is an eigenpair of $A A^t \\Rightarrow (\\lambda, A^t x)$ is an eigenpair of $A^t A.$**\n",
    "</div>\n",
    "<div style=\"float:right;width:11cm;height:4.5cm;border:1px solid black;padding-left:2mm;\">\n",
    "\n",
    "$\\begin{align}\n",
    "\\left( \\lVert A x \\rVert^2 \\right)&  = \\left( (A x) \\cdot (A x) \\right) \\quad \\text{ for eigenpair } (\\lambda, x) \\text{ of } A^t A\\\\\n",
    "& = (A x)^t (A x) \\\\\n",
    "& = x^t A^t A x \\\\\n",
    "& = \\lambda x^t x \\\\\n",
    "& = \\lambda \\left( \\lVert x \\rVert^2 \\right) \\ne 0.\n",
    "\\end{align}$\n",
    "\n",
    "$\\;\\;$ Since $x$ is an eigenvector $\\lVert x \\rVert \\ne 0 . \\quad$\n",
    "$\\therefore \\lambda \\ne 0 \\Rightarrow \\color{red}{A x \\ne 0}.$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true"
   },
   "source": [
    "<div style=\"float:left;width:14.5cm;height:4.8cm;border:1px solid black;\">\n",
    "\n",
    "* For the eigenspaces for eigenvalue $\\lambda = 0$, we have\n",
    "    * for $A^t A$, we have $dim\\ E_0 = N - rank(A)$\n",
    "    * for $A A^t$, we have $dim\\ E_0 = M - rank(A)$\n",
    "<br><br>\n",
    "\n",
    "* Both matrices have the **same dimension for the eigenspaces $dim\\ E_\\lambda$ for $\\lambda \\ne 0$**\n",
    "</div>\n",
    "<div style=\"float:right;width:11cm;height:4.8cm;border:1px solid black;padding-left:2mm;\">\n",
    "\n",
    "Let $(\\lambda, x_1), (\\lambda, x_2)$ be eigenpairs of $A^t A$,<br>\n",
    "$\\quad$ with linearly independent eigenvectors $x_1, x_2$ and $\\lambda \\ne 0$.\n",
    "\n",
    "$\\begin{align}\n",
    "\\alpha A x_1 + \\beta A x_2 = 0\n",
    "& \\; \\Rightarrow \\; & \\alpha A^t A x + \\beta A^t A x = 0 \\\\\n",
    "& \\; \\Rightarrow \\; & \\alpha \\lambda x + \\beta \\lambda x = 0 \\\\\n",
    "& \\; \\Rightarrow \\; & \\lambda ( \\alpha x + \\beta x ) = 0 \\\\\n",
    "& \\; \\Rightarrow \\; & \\alpha = \\beta = 0 \\\\\n",
    "\\end{align}$\n",
    "    \n",
    "$\\therefore$ the eigenvectors $A x_1$ and $A x_2$ are linearly independent. \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "##### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;height:3.5cm;\">\n",
    "$\\quad A = \\frac{1}{125}\\ \\left(\n",
    "\\begin{array}{rrrrr}\n",
    "66 & -88 & -66 & 20 & 88 \\\\\n",
    "60 & -80 & 15 & -50 & -20 \\\\\n",
    "12 & -16 & -12 & -110 & 16 \\\\\n",
    "120 & -160 & 30 & 25 & -40 \\\\\n",
    "\\end{array}\n",
    "\\right)$\n",
    "</div>\n",
    "<div style=\"float:left;padding-left:3cm;\">\n",
    "<strong>Same non-zero eigenvalues with same multiplicities:</strong><br>\n",
    "$\\qquad A^t A$ has size $5 \\times 5,\\;$ with eigenvalues $\\quad 4,\\ 1,\\ 1,\\ \\color{red}0,\\ \\color{red}0$<br>\n",
    "$\\qquad A A^t$ has size $4 \\times 4,\\;$ with eigenvalues $\\quad 4,\\ 1,\\ 1,\\ \\color{red}0$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\qquad v_1 = \\left(\\begin{array}{r} \\;\\;0 \\\\ 0 \\\\ 0 \\\\ 1 \\\\ 0 \\end{array}\\right), \\quad u_1 = A v_1 = \\frac{1}{125} \\left(\\begin{array}{r} 20\\\\ -50\\\\ -110 \\\\ 25 \\end{array}\\right) \\quad$ are eigenvectors with eigenvalues $\\lambda =1$ for $A^t A$ and $A A^t$ respectively"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly\n",
    "\n",
    "$\\qquad v_2 = \\frac{1}{5} \\begin{pmatrix} 0 \\\\ 0 \\\\ 3 \\\\ 0 \\\\ -4 \\end{pmatrix}, \\quad u_2 = A v_2 = \\left(\\begin{array}{r} 0.88 \\\\ -0.20 \\\\ 0.16 \\\\ -0.40 \\end{array}\\right) \\quad$ are eigenvectors with eigenvalues $\\lambda =1$  for $A^t A$ and $A A^t$ respectively"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Checking linear independence, we see that both $\\{ v_1, v_2 \\}$ and $\\{ u_1, u_2 \\}$ are linearly independent."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 1.4 Orthogonality of Eigenvectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "false"
   },
   "source": [
    "Let $(\\lambda_1, x_1)$ and $(\\lambda_2, x_2)$ be eigenpairs of $A^t A$.<br>\n",
    "$\\quad\\quad$ If $x_1 \\perp x_2$, we find\n",
    "$\\quad\n",
    "((A x_1) \\cdot (A x_2)) = x_1^t A^t A x_2 = ( \\lambda_2\\; x_1 \\cdot x_2 ) = 0.\n",
    "$\n",
    "\n",
    "$\\quad\\quad$ Since $A x_1$,$A x_2$ are eigenvectors of $A A^t$ provided $\\lambda_1 \\lambda_2 \\ne 0,$ we find<br>\n",
    "<div style=\"padding-left:2cm;\">\n",
    "\n",
    "* **Given eigenpairs $(\\lambda_1 \\ne 0, x_1)$ and $(\\lambda_2 \\ne 0, x_2)$ of $A^t A$,\n",
    "  then $(\\lambda_1, A x_1), (\\lambda_2 A x_2)$ are eigenpairs of $A A^t.$<br>\n",
    "  $\\quad\\quad$ Further, $x_1 \\perp x_2 \\Rightarrow A x_1 \\perp A x_2.$**\n",
    "* **Given eigenpairs $(\\lambda_1 \\ne 0, x_1)$ and $(\\lambda_2 \\ne 0, x_2)$ of $A A^t$,\n",
    "  then $(\\lambda_1, A^t x_1), (\\lambda_2 A^t x_2)$ are eigenpairs of $A A^t.$<br>\n",
    "  $\\quad\\quad$ Further, $x_1 \\perp x_2 \\Rightarrow A^t x_1 \\perp A^t x_2.$**\n",
    "\n",
    "**Remark:**  The eigenvalues $\\lambda_1$ and $\\lambda_2$ need not be distinct.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 1.5 Eigenvalues Cannot be Negative"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true",
    "tags": []
   },
   "source": [
    "Given an eigenpair $(\\lambda, x)$ of $A^t A:$\n",
    "\n",
    "$\\qquad\n",
    "\\begin{align}\n",
    "A^t A x = \\lambda x\n",
    "& \\Rightarrow x^t A^t A x = \\lambda x^t x \\\\\n",
    "& \\Rightarrow \\lVert A x \\rVert^2 = \\lambda \\lVert x \\rVert^2\n",
    "& \\Rightarrow \\lambda \\ge 0\\quad\\text{ since } x \\ne 0.\n",
    "\\end{align}$\n",
    "\n",
    "$\\qquad$ **The matrices $A^t A$ and $A A^t$ are positive semidefinite.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 1.6 Eigenvector Lengths"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:14cm;height:3.5cm;border:1px solid black;padding-left:2mm;padding-right:1cm;\">\n",
    "\n",
    "Let $\\mathbf{(\\lambda, v)}$ be an eigenpair of $A^t A$ with $\\lambda \\ne 0,\\; \\lVert v \\rVert = 1$<br>\n",
    "$\\qquad$ $\\mathbf{(\\lambda, u = \\frac{1}{\\sqrt{\\lambda}} A v )}\\;\\;$ is a corresponding eigenpair of $A A^t$ with\n",
    "$\\lVert u \\rVert = 1$.\n",
    "\n",
    "Let $\\mathbf{(\\lambda, u)}$ be an eigenpair of $A A^t$ with $\\lambda \\ne 0,\\; \\lVert u \\rVert = 1$<br>\n",
    "$\\qquad$ $\\mathbf{(\\lambda, v = \\frac{1}{\\sqrt{\\lambda}} A^t u )}\\;\\;$ is a corresponding eigenpair of $A^t A$ with\n",
    "$\\lVert v \\rVert = 1$.\n",
    "</div>\n",
    "<div style=\"float:right;width:10cm;height:3.5cm;border:1px solid black;padding: 0cm 1cmm 0cm 0cm;\">\n",
    "$\\begin{align}\n",
    "    \\lVert A v \\rVert^2 =&\\; ( A v )^t ( A v )\\\\\n",
    "                        =&\\; v^t A^t A v \\\\\n",
    "                        =&\\; \\lambda v^t v \\\\\n",
    "                        =&\\; \\lambda \\lVert v \\rVert^2 \\\\\n",
    "                        =&\\; \\lambda\n",
    "\\end{align}$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 2. Symmetric Eigendecomposition of $A^t A$ and $A A^t$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Eigenvectors of $A^t A$ for eigenvalue $\\lambda = 0$ are in the null space $\\mathscr{N}( A^t A ) = \\mathscr{N}(A)$.<br>\n",
    "Eigenvectors of $A^t A$ for eigenvalues $\\lambda \\ne 0$ are in the orthogonal complement $\\mathscr{N}^\\perp(A^t A) = \\mathscr{N}^\\perp(A) = \\mathscr{R}(A)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "When we write basis vectors for these spaces into a matrix $Q$ as columns, we can choose to order them:\n",
    "* eigenvectors for larger eigenvalues to the left of eigenvectors for smaller eigenvalues,<br>\n",
    "  eigenvectors for eigenvalue 0 last.\n",
    "* $Q$ then has two sets of columns\n",
    "    * The first $r$ columns, where $r = rank(A)$, are a basis for $\\mathscr{R}(A)$\n",
    "    * The remaining columns are a basis for $\\mathscr{N}(A)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same reasoning applies to $A A^t$ and its eigenvector matrix $Q$\n",
    "* $Q$ has two sets of columns\n",
    "    * The first $r$ columns, where $r = rank(A)$, are a basis for $\\mathscr{C}(A)$\n",
    "    * The remaining columns are a basis for $\\mathscr{N}(A^t)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "For matrix $A$ of size $M \\times N,$ The diagonal matrix has the form\n",
    "<div style=\"float:left;width:30%;\">\n",
    "\n",
    "* $\\Lambda = \\begin{pmatrix} \\Lambda_r & 0 \\\\ 0 & 0 \\end{pmatrix}$ of size $N \\times N$ for $A^t A$\n",
    "* $\\Lambda = \\begin{pmatrix} \\Lambda_r & 0 \\\\ 0 & 0 \\end{pmatrix}$ of size $M \\times M$ for $A A^t$\n",
    "</div>\n",
    "<div style=\"float:left;width:40%;\">\n",
    "\n",
    "where $\\quad \\Lambda_r = \\begin{pmatrix}\n",
    "\\lambda_1 & 0 & \\dots & 0 \\\\\n",
    "0     & \\lambda_2 &  \\dots & 0 \\\\\n",
    "\\dots & \\dots     &  \\dots & \\dots \\\\\n",
    "0 & 0 & \\dots & \\lambda_r\n",
    "\\end{pmatrix}, \\quad$ and  $r = rank A$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider $A = \\frac{1}{115} \\left(\n",
    "\\begin{array}{rrrrr}\n",
    "-6 & 0 & -48 & 36 & -158 \\\\\n",
    "-164 & 0 & -24 & 18 & -102 \\\\\n",
    "-24 & 0 & 176 & -132 & -57 \\\\\n",
    "\\end{array}\\right), \\quad$ so that $A^t A$ has size $5 \\times 5$ and $A A^t$ has size $3 \\times 3$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "##### **Orthogonal Eigendecomposision of $A^t A$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then $\\qquad A^t A = \\frac{1}{25}\\ \\left(\n",
    "\\begin{array}{rrrrr}\n",
    "52 & 0 & 0 & 0 & 36 \\\\\n",
    "0 & 0 & 0 & 0 & 0 \\\\\n",
    "0 & 0 & 64 & -48 & 0 \\\\\n",
    "0 & 0 & -48 & 36 & 0 \\\\\n",
    "36 & 0 & 0 & 0 & 73 \\\\\n",
    "\\end{array}\n",
    "\\right)\\quad$ has eigenvalues $4,4,1,0,0$   and therefore rank(A) = 3."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "###### **Orthogonal Matrix $V$, Diagonal Matrix $\\Lambda$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The orthogonal eigendecomposition of this matrix is given by $A^t A = V \\Lambda V^t,\\;$ where"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\qquad$ the orthonormal eigenvector matrix is\n",
    "$\\quad V = \\left( V_r \\mid \\tilde{V}_r \\right) = \\frac{1}{10} \\left(\n",
    "\\begin{array}{rrr|rr}\n",
    "6 & 0 & 8 & 0 & 0  \\\\\n",
    "0 & 0 & 0 & 0 & 10 \\\\\n",
    "0 & 8 & 0 & 6 & 0  \\\\\n",
    "0 & -6 & 0 & 8 & 0 \\\\\n",
    "8 & 0 & -6 & 0 & 0 \\\\\n",
    "\\end{array}\n",
    "\\right)\\quad$\n",
    "and $\\quad$\n",
    "$\\Lambda = \\begin{pmatrix} \\Lambda_r & 0 \\\\ 0 & 0 \\end{pmatrix} = \\left(\n",
    "\\begin{array}{ccc|cc}\n",
    "\\color{red}4 & 0 & 0 & 0 & 0 \\\\\n",
    "0 & \\color{red}4 & 0 & 0 & 0 \\\\\n",
    "0 & 0 & \\color{red}1 & 0 & 0 \\\\ \\hline\n",
    "0 & 0 & 0 & \\color{red}0 & 0 \\\\\n",
    "0 & 0 & 0 & 0 & \\color{red}0 \\\\\n",
    "\\end{array}\n",
    "\\right)\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The first 3 columns of $V$ (i.e., the submatrix $V_r$) form an orthonormal basis for $\\mathscr{R}(A)$,<br>\n",
    "$\\quad$ the remaining two columns (i.e., the submatrix $\\tilde{V}_r$ form an orthonormal basis for $\\mathscr{N}(A)$.\n",
    "* The non-zero eigenvalues in $\\Lambda$ are arranged in decreasing order and define a nonzero submatrix $\\Lambda_r$ of size $3 \\times 3$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "##### **Orthogonal Eigendecomposition of $A A^t$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's reuse the computations for $A^t A$ as much as possible to compute an orthogonal decomposition $A A^t = U \\Lambda U^t$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The matrix is size $3\\times 3$, so there are two one less eigenvalues: $\\lambda = 4,4,1 : \\quad$ $\\Lambda_r$ stays the same.<br>\n",
    "$\\quad$ The resulting $\\Lambda$ matrix is size $4 \\times 4$<br>\n",
    "\n",
    "$\\qquad\\quad$\n",
    "$\\Lambda = \\begin{pmatrix} \\color{red}{\\Lambda_r} & 0 \\\\ 0 & 0 \\end{pmatrix} =\n",
    "\\Lambda_r =\n",
    "\\left(\n",
    "\\begin{array}{ccc}\n",
    "\\color{red}4 & 0 & 0 \\\\\n",
    "0 & \\color{red}4 & 0 \\\\\n",
    "0 & 0 & \\color{red}1\n",
    "\\end{array}\n",
    "\\right).\n",
    "\\quad$  Since there are no zero eigenvalues, this is just $\\Lambda_r$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The matrix $U = (U_r \\mid \\tilde{U_r})$ consisting of orthonormal basis vectors for $\\mathscr{C}(A)$ and $\\mathscr{N}(A^t)$ in $U_r$ and $\\tilde{U}_r$ respectively.<br><br>\n",
    "The eigenvectors in the columns of $U_r$ can be obtained from the eigenvectors in the columns of $V_r$ by $u_i = \\frac{1}{\\lambda_i} A v_i$<br>\n",
    "$\\quad$ (the square root scale factor creates vectors of unit length):<br>\n",
    "$\\qquad\\quad U_r = A V_r \\Lambda_r^{-\\frac{1}{2}} \\approx \\left(\n",
    "\\begin{array}{ccc}\n",
    "0.26 & 0.57 & 0.78 \\\\\n",
    "0.13 & 0.78 & -0.61 \\\\\n",
    "-0.96 & 0.26 & 0.13 \\\\\n",
    "\\end{array}\n",
    "\\right)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* We still need the remaining $\\tilde{U}_r$ matrix. Its collumns are an orthonormal set of vectors in $\\mathscr{N}(A^t) = \\mathscr{N}(A A^t) = \\mathscr{N}(U_r^t)$<br>\n",
    "$\\quad$ In this example, the dimension of this space is 0, so the columns of $\\tilde{U}_r$ are the empty set.<br><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* We have obtained the orthogonal decomposition $\\qquad\\quad A A^t = U_r \\Lambda_r U^t_r$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Gram Matrix $A^t A$ has a number of useful properties:\n",
    "* $A^t A$ is symmetric: it has an orthogonal decomposition\n",
    "* $A^t A$ does not have negative eigenvalues: it is positive semi-definite"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The orthogonal eigendecompositions of $A^t A$ and $A A^t$ are related. For $A$ of sizem $M \\times N$ with $rank A = r$\n",
    "* $A^t A = \\left( V_r \\mid \\tilde{V}_r \\right)\n",
    "          \\left( \\begin{array}{c|c} \\Lambda_r & 0 \\\\ \\hline 0 & 0 \\end{array} \\right)\n",
    "          \\left( \\begin{array}{c} V^t_r           \\\\ \\hline \\tilde{V}_r^t \\end{array} \\right) = V_r \\Lambda_r V_r^t$<br>\n",
    "    * $V_r$ has size $M \\times r$,  $V_r^t V_r = I$ with columns forming a basis for $\\mathscr{R}(A)$\n",
    "    * $\\Lambda_r$ is an invertible diagonal matrix of size $r \\times r$\n",
    "    * $\\tilde{V}_r$ has size $M \\times r$, $\\tilde{V}^t_r \\tilde{V} = I$ with columns forming a basis for $\\mathscr{N}(A)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* $A A^t = \\left( U_r \\mid \\tilde{U}_r \\right)\n",
    "          \\left( \\begin{array}{c|c} \\Lambda_r & 0 \\\\ \\hline 0 & 0 \\end{array} \\right)\n",
    "\\left( \\begin{array}{c} U^t_r           \\\\ \\hline \\tilde{U}_r^t \\end{array} \\right) = U_r \\Lambda_r U_r^t$<br>\n",
    "    * $U_r$ has size $N \\times r$,  $U_r^t U_r = I$ with columns forming a basis for $\\mathscr{C}(A)$\n",
    "    * $\\Lambda_r$ is an invertible diagonal matrix of size $r \\times r$\n",
    "    * $\\tilde{U}_r$ has size $N \\times r$, $\\tilde{U}^t_r \\tilde{U} = I$ with columns forming a basis for $\\mathscr{N}(A^t)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* By convention, the **diagonal entries** of $\\Lambda_r$ are the non-zero eigenvalues of $A^t A$ and $A A^t$ in **decreasing order**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* $u_i = \\frac{1}{\\sqrt{\\lambda_i}} A v_i, i=1,2, \\dots r$."
   ]
  }
 ],
 "metadata": {
  "@webio": {
   "lastCommId": "34c68ec4-442b-4fdd-b87d-255a89932dcd",
   "lastKernelId": "8d878c67-9c5e-4735-83f3-8db5dce6c425"
  },
  "kernelspec": {
   "display_name": "Julia 1.5.4",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
