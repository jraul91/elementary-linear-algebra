{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "using LinearAlgebra, LaTeXStrings, Plots, LAcode"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fitting a nonlinear-resistance model\n",
    "\n",
    "[Ohm's law](https://en.wikipedia.org/wiki/Ohm%27s_law) says that voltage drop $d$ across a resistor is proportional to the current $i$ via $d = iR$, where $R$ is the [resistance](https://en.wikipedia.org/wiki/Electrical_resistance_and_conductance).  Normally, we treat $R$ as a constant (the relationship is *linear*), but in fact this is not exactly true.\n",
    "\n",
    "A real resistance actually *changes* as you increase the voltage drop $d$.  (For example, this happens as the resistor heats up; eventually it melts!)\n",
    "One simple model of a nonlinear resistance is:\n",
    "$$\n",
    "R = x_1 + x_2 d^2 .\n",
    "$$\n",
    "Here, the resistance is constant plus a \"small\" correction that grows as we increase $d$.  (We won't have any term proportional to $d$ if the current flows equally well in both directions, so that $R$ doesn't depend on the *sign* of $d$.)\n",
    "\n",
    "One would normally get these coefficients $x_1$ and $x_2$ by experimental measurements.  Such \"fitting\" processes lead directly into the next major topic in 18.06:\n",
    "\n",
    "## Exact fits\n",
    "\n",
    "Suppose we put two voltage differences $d_1$ and $d_2$ across our resistor, and measure two resistances $R_1$ and $R_2$.  This leads to the following $2 \\times 2$ system of equations for the unknown coefficients $x_1$ and $x_2$:\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix} x_1 + x_2 d_1^2 \\\\ x_1 + x_2 d_2^2 \\end{pmatrix} =\n",
    "\\underbrace{\\begin{pmatrix} 1 & d_1^2 \\\\ 1 & d_2^2 \\end{pmatrix}}_A\n",
    "\\underbrace{\\begin{pmatrix} x_1 \\\\ x_2 \\end{pmatrix}}_x =\n",
    "\\underbrace{\\begin{pmatrix} R_1 \\\\ R_2 \\end{pmatrix}}_b \\; ,\n",
    "$$\n",
    "\n",
    "i.e. a system $Ax = b$.   As long as $|d_1| \\ne |d_2|$, the matrix $A$ is non-singular, and this has a unique solution: an **interpolating polynomial** going through the two data points:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "let fig = plot(xrange=(0,3),yrange=(0,3)), d = range(0,stop=3,length=100)\n",
    "for d₁=0:0.1:1, d₂=1.1:0.1:2, R₁=1:0.1:2, R₂=1:0.1:2\n",
    "        x = [1 d₁^2; 1 d₂^2] \\ [R₁, R₂]\n",
    "        plot!([d₁, d₂], [R₁, R₂])\n",
    "        plot!(d, x[1] .+ x[2]*d.^2)\n",
    "        #title(L\"fit\\ \\$x\\$ = $x\")\n",
    "        #xlabel(L\"d\")\n",
    "        #ylabel(L\"R\")\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot!()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general, if you have $m$ points you can interpolate them in this way by a degree $m-1$ polynomial (called the [Lagrange interpolating polynomial](https://en.wikipedia.org/wiki/Lagrange_polynomial)) in this way: you set up an $m \\times m$ matrix system for the coefficients.\n",
    "\n",
    "Note, however, that this is neither the most efficient nor the most accurate (given roundoff errors) method to compute an exact interpolating polynomial.  There is a much better method called [barycentric interpolation](https://people.maths.ox.ac.uk/trefethen/barycentric.pdf) that is outside the scope of 18.06."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inexact fits\n",
    "\n",
    "Suppose we do more than just the bare minimum number of measurements, however.  Suppose that we do $m > 2$ measurements.  This gives the following system of equations:\n",
    "\n",
    "\n",
    "\n",
    "$$\n",
    "\\underbrace{\\begin{pmatrix} 1 & d_1^2 \\\\ 1 & d_2^2 \\\\\n",
    "                           \\vdots & \\vdots  \\\\ 1 & d_m^2 \\end{pmatrix}}_A\n",
    "\\underbrace{\\begin{pmatrix} x_1 \\\\ x_2 \\end{pmatrix}}_x =\n",
    "\\underbrace{\\begin{pmatrix} R_1 \\\\ R_2 \\\\ \\vdots \\\\ R_m \\end{pmatrix}}_b \\; ,\n",
    "$$\n",
    "\n",
    "Now, $A$ is $m \\times 2$, and full column rank (assuming distinct voltages $|d_k|$), but of course it is not invertible for $m > 2$.\n",
    "\n",
    "It will still have a solution (a unique solution!) if all of the $R_k$ measurements fall *exactly* on a quadratic curve $x_1 + x_2 d^2$, but in a real experiment there would be some *measurement noise* that would spoil this.\n",
    "\n",
    "For example, let's suppose $x = (1,2)$ and we do $m=200$ measurements for $d \\in [0,2]$, but that each measurement has a random uncertainty $\\approx R \\pm 0.1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d = range(0,stop=2,length=200) # 20 points from 1 to 4\n",
    "b = 1 .+ 2*d.^2 .+ randn(200)*0.1 # measurements with Gaussian random noise\n",
    "scatter(d, b, title=L\"noisy \\\"measurements\\\" of \\$R = 1 + d^2\\$\")\n",
    "#xlabel(L\"d\")\n",
    "#ylabel(L\"R\")\n",
    "#title(\"noisy \\\"measurements\\\" of \\$R = 1 + d^2\\$\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The corresponding matrix $A$ is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "collect(d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = [ones(d) d.^2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rank(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$A$ doesn't have an inverse, of course:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "inv(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, if we blindly do `A \\ b`, it seems to give us quite a reasonable $x$, very close to the exact $x = (1,2)$ of the underlying data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x̂ = A \\ b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is it doing?  Let's plot the curve from the coefficients `A \\ b`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d = linspace(0,2,200) # 20 points from 1 to 4\n",
    "b = 1 + 2*d.^2 + randn(200)*0.1 # measurements with Gaussian random noise\n",
    "plot(d, b, \"r.\")\n",
    "plot(d, x̂[1] + x̂[2] * d.^2, \"k-\")\n",
    "xlabel(L\"d\")\n",
    "ylabel(L\"R\")\n",
    "title(\"Fit A\\\\b to noisy \\\"measurements\\\" of \\$R = 1 + d^2\\$\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "norm(b - A*x̂) / norm(b)  # the length of the \"residual\" b - Ax̂ is not zero!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This $\\hat{x}$ is *not* an exact solution: $A\\hat{x} \\ne b$: the \"residual\" (the \"error\") $b - A\\hat{x}$ is not zero, as we can see by checking its norm $\\Vert b - A\\hat{x}\\Vert$ above.\n",
    "\n",
    "In the plot above, correspondingly, the black \"fit\" curve does *not* exactly match the data points.  But it is pretty close!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Least-square fits\n",
    "\n",
    "What `A \\ b` is doing in Julia, for a non-square \"tall\" matrix $A$ as above, is computing a **least-square fit** that **minimizes the sum of the square of the errors**.   This is an extremely important thing to do in many areas of linear algebra, statistics, engineering, science, finance, etcetera.\n",
    "\n",
    "Above, we have a bunch of measurements $(d_k, R_k)$, and we are trying to *fit* it to a function $R(x,d) = x_1 + x_2 d^2$.   There is no exact fit, so instead we minimize the sum of the squares of the errors:\n",
    "\n",
    "$$\n",
    "\\min_{x \\in \\mathbb{R}^2} \\sum_{k=1}^m (R_k - R(x, d_k))^2 \\;.\n",
    "$$\n",
    "\n",
    "Let's write this in terms of linear-algebra operations.  We have our $m \\times 2$ matrix $A$ above, and in terms of this we have $R(x, d_k) = (Ax)_k$: the \"fit\" function at $d_k$ is the $k$-th row of $Ax$.  But then $R_k - R(x,d_k)$ is the $k$-th row of the **residual vector**\n",
    "\n",
    "$$\n",
    "\\mbox{residual} = b - Ax\n",
    "$$\n",
    "\n",
    "and we can see that our sum above is precisely the **sum of the squares of the residual components**, which is the **square of the length of the residual**:\n",
    "\n",
    "$$\n",
    "\\sum_{k=1}^m (R_k - R(x, d_k))^2 = \\Vert b - Ax \\Vert^2 \\; .\n",
    "$$\n",
    "\n",
    "Here, $\\Vert y \\Vert = \\sqrt{y^T y}$ is the *length* of a vector, also called the **norm of the vector**: the square root of the dot product with itself.  (More specifically, this is the [Euclidean norm](https://en.wikipedia.org/wiki/Euclidean_distance), also called the $L^2$ norm.  There are other ways to define a vector norm, but I think this is the only one we will use in 18.06.)\n",
    "\n",
    "So, what we are *really* doing is **minimizing the norm of the residual**:\n",
    "\n",
    "$$\n",
    "\\boxed{\n",
    "\\min_{x \\in \\mathbb{R}^2} \\Vert b - Ax \\Vert\n",
    "}\n",
    "$$\n",
    "\n",
    "(Note that minimizing the norm and minimizing the squared norm will give the same solution $x$; which one we write is a matter of convenience.)\n",
    "\n",
    "This is exactly what `A \\ b` does in Julia (and Matlab) for a non-square matrix $A$, which is why it gives us a good fit above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Least-squares fits and the normal equations\n",
    "\n",
    "As derived in section 4.3 of the Strang textbook, minimizing $\\Vert b - Ax\\Vert$ or (equivalently) $\\Vert b - Ax\\Vert^2$ leads to the so-called **\"normal equations\"** for the minimizer $\\hat{x}$:\n",
    "\n",
    "$$\n",
    "\\boxed{ A^T A \\hat{x} = A^T b }\n",
    "$$\n",
    "\n",
    "These *always* have a solution.  If $A$ is an $m \\times n$ matrix, then $A^T A$ is $n \\times n$, and:\n",
    "\n",
    "* Usually in fitting problems, $A$ is a \"tall\" matrix with full column rank $n$, in which case $\\operatorname{rank}(A^T A) = \\operatorname{rank}(A) = n$ and $A^T A$ is *invertible*: the solution $\\hat{x}$ exists and is unique.\n",
    "\n",
    "* Even if $\\operatorname{rank}(A) < n$, we still have a (non-unique) solution, because $A^T b \\in C(A^T) = N(A)^\\perp = N(A^T A)^\\perp = C(A^T A)$.\n",
    "\n",
    "Now (on the blackboard), I will show that we can derive the normal equations directly by 18.02.  If we just take the partial derivatives of $f(x) = \\Vert b - Ax \\Vert^2$ and set them to zero, we find that $0 = \\nabla_x f = 2A^T Ax - 2A^T b$ and the normal equations follow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More polynomial fitting examples.\n",
    "\n",
    "Suppose we are fitting m points $(a_k,b_k)$ to a degree-(n+1) polynomial of the form\n",
    "\n",
    "$$\n",
    "p(a) = x_1 + x_2 a + x_3 a^2 + \\cdots + x_n a^{n-1} \\; ,\n",
    "$$\n",
    "\n",
    "which leads to the $m \\times n$ matrix\n",
    "\n",
    "$$\n",
    "A = \\begin{pmatrix}\n",
    "        1 & a_1 & a_1^2 & \\cdots a_1^{n-1} \\\\\n",
    "        1 & a_2 & a_2^2 & \\cdots a_2^{n-1} \\\\\n",
    "        1 & a_3 & a_3^2 & \\cdots a_3^{n-1} \\\\\n",
    "        \\vdots & \\vdots & \\vdots & \\vdots \\\\\n",
    "        1 & a_m & a_m^2 & \\cdots a_m^{n-1} \\\\\n",
    "    \\end{pmatrix}\n",
    "$$\n",
    "\n",
    "and hence to the normal equations for the fit coefficients $\\hat{x}$.\n",
    "\n",
    "Let's generate 50 data points from a degree-3 polynomial $1 + 2a + 3a^2 + 4a^3$ plus noise, and see what happens as we change the fitting degree $n$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using Interact\n",
    "\n",
    "let a = range(0,stop=1.5,length=50),\n",
    "    afine = range(0,stop=1.5,length=1000),\n",
    "    b = 1 .+ 2a .+ 3a.^2 .+ 4a.^3 .+ randn(length(a)),\n",
    "    @manipulate for n=slider(1:40, value=2)\n",
    "            p = scatter(a, b, title=L\"noisy cubic: least-square fit of degree $(n-1)\")\n",
    "            A = a .^ (0:n-1)'\n",
    "            x̂ = A \\ b\n",
    "            plot!(afine, (afine .^ (0:n-1)') * x̂, xrange=(0,1.6), yrange=(-5,30))\n",
    "        end\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Increasing the degree of the fit at first seems to improve things, up to about degree 6, but then it becomes more and more wiggly.  It is still reducing the residual, but clearly it is \"fitting noise\" and the error actually becomes worse compared to the \"real\" underlying cubic model.  This problem is called [overfitting](https://en.wikipedia.org/wiki/Overfitting).\n",
    "\n",
    "In between the fitted points, especially near the edges, the wiggles can actually diverge as we increase the degree, an effect related to what is known as a [Runge phenomenon](https://en.wikipedia.org/wiki/Runge's_phenomenon). Even if there is *no noise*, fitting *equally spaced points* to high-degree polynomials can lead to disaster if the underlying data is not exactly polynomial.  Let's demonstrate this by fitting the *smooth* function $b(a) = 1/(1+25a^2)$ to polynomials at 50 points:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "let a = linspace(-1,1,50),\n",
    "    afine = linspace(-1,1,1000),\n",
    "    b = 1 ./ (1 + 25 * a.^2),\n",
    "    fig = figure()\n",
    "    @manipulate for n=slider(1:50, value=3)\n",
    "        withfig(fig) do\n",
    "            plot(a, b, \"r.\")\n",
    "            A = a .^ (0:n-1)'\n",
    "            x̂ = A \\ b\n",
    "            plot(afine, (afine .^ (0:n-1)') * x̂, \"k-\")\n",
    "            xlabel(L\"a\")\n",
    "            ylabel(L\"b\")\n",
    "            xlim(-1,1)\n",
    "            ylim(0,1)\n",
    "            title(\"smooth function: polynomial fit of degree $(n-1)\")\n",
    "        end\n",
    "    end\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Be careful not to draw the wrong lesson from this.   You have to be *especially careful* when fitting to high-degree polynomials, but this does *not* mean that you should never do it.\n",
    "\n",
    "In particular, fitting smooth functions to high-degree polynomials can be a *great* thing to do, as **long as you choose the correct points**.  (Separately, as mentioned above, the construction of the interpolating polynomial should technically not be done by this matrix method once you go to high degrees — you run into roundoff-error problems — but there are better methods like the barycentric formula.)\n",
    "\n",
    "For example, here we fit the same $1/(1+25a^2)$ function as above to a degree-50 polynomial, with no Runge problems at all, by choosing the points to be [Chebyshev nodes](https://en.wikipedia.org/wiki/Chebyshev_nodes) (which cluster together at the edges of the domain):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 51\n",
    "a = @. cos(((1:n)-0.5) * pi/n) # = Chebyshev nodes of order n\n",
    "afine = linspace(-1,1,1000)\n",
    "b = 1 ./ (1 + 25 * a.^2)\n",
    "plot(a, b, \"r.\")\n",
    "A = a .^ (0:n-1)'\n",
    "x̂ = A \\ b\n",
    "plot(afine, (afine .^ (0:n-1)') * x̂, \"k-\")\n",
    "xlabel(L\"a\")\n",
    "ylabel(L\"b\")\n",
    "xlim(-1,1)\n",
    "ylim(0,1)\n",
    "title(\"smooth function: Chebyshev polynomial interpolation of degree $(n-1)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How this works goes far outside the bounds of 18.06, but is beautiful and fascinating mathematics.  See, for example, [this book](https://people.maths.ox.ac.uk/trefethen/ATAP/) and [these video lectures](https://people.maths.ox.ac.uk/trefethen/atapvideos.html) by [Nick Trefethen](https://people.maths.ox.ac.uk/trefethen/).\n",
    "\n",
    "The great thing about fitting complicated functions to polynomials is that polynomials are usually easier to work with — finding roots, derivatives, and integrals of polynomials is easy, for example.   A pioneering software package encapsulating this idea is [chebfun](http://www.chebfun.org/), and a Julia implementation of similar ideas is [ApproxFun](https://github.com/JuliaApproximation/ApproxFun.jl)."
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Julia 1.5.3",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.3"
  },
  "widgets": {
   "state": {
    "0830e586-a8c8-4ed4-93ee-ac3243f41542": {
     "views": [
      {
       "cell_index": 2
      }
     ]
    },
    "27349d0c-9119-4fdb-be2c-04717452ec18": {
     "views": [
      {
       "cell_index": 2
      }
     ]
    },
    "34d7b83b-e4fe-43fa-8919-b70077d8263e": {
     "views": [
      {
       "cell_index": 2
      }
     ]
    },
    "36d1ded7-edb8-4e09-91b6-dfbd46ffdbeb": {
     "views": [
      {
       "cell_index": 19
      }
     ]
    },
    "58a292a0-1cde-4194-b267-e87802fdf709": {
     "views": [
      {
       "cell_index": 21
      }
     ]
    },
    "8c46751f-88fc-4e50-a563-f4fcb4e4acb3": {
     "views": [
      {
       "cell_index": 2
      }
     ]
    },
    "8f459522-861e-4949-88ff-b2f73d719ef0": {
     "views": [
      {
       "cell_index": 2
      }
     ]
    },
    "9c01dcd5-b682-4df6-82b0-05e1cc7c6dce": {
     "views": [
      {
       "cell_index": 2
      }
     ]
    },
    "cde5b1cb-7a8f-48fb-97e4-7d1e3287b502": {
     "views": [
      {
       "cell_index": 2
      }
     ]
    },
    "d011e795-82d1-48db-93d6-7af4d3adbbd7": {
     "views": [
      {
       "cell_index": 2
      }
     ]
    }
   },
   "version": "1.2.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
