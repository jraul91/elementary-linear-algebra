{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "using LinearAlgebra, Plots"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"width:100%;color:darkred;text-align:center;font-size:45px;height:45px;\"><strong>Metric Spaces</strong></div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "html\"<iframe width=\\\"400\\\" height=\\\"200\\\" src=\\\"https://www.youtube.com/embed/-fQlMWWnp_g\\\"  frameborder=\\\"0\\\" allow=\\\"accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture\\\" allowfullscreen></iframe>\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Inner Products"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 Inner Products and Metrics in $\\mathbb{R}^n$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Motivation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The notion of \"distance\" is not restricted to Euclidean length. E.g.,\n",
    "* we might be interested in travel time\n",
    "* different directions might be more difficult<br> (climb up/ stay on the valley floor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use the **notation $\\quad <u,v> \\quad $ for the inner product,** rather than $\\quad u \\cdot v$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Examples:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Different cost along the coordinate axes\n",
    "> * $\n",
    "u=\\begin{pmatrix} u_1 \\\\ u_2 \\\\ u_3 \\end{pmatrix}, \\quad\n",
    "v=\\begin{pmatrix} v_1 \\\\ v_2 \\\\ v_3 \\end{pmatrix}, \\quad\n",
    "< u, v > = 3\\ u_1 v_1  + 2\\ u_2 v_2 +\\ u_3 v_3.\n",
    "$\n",
    ">\n",
    "> * **Norm:**   $\\quad\\; \\lVert{u} \\rVert = \\sqrt{\\ 3\\ u_1^2  + 2\\ u_2^2 +\\ u_3^2 } $\n",
    "> * **Metric:** $\\quad d(u,v) = \\lVert v-u \\rVert.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Different cost in some direction\n",
    ">$\n",
    "u \\in \\mathbb{R}^n, \\quad\n",
    "u \\in \\mathbb{R}^n, \\quad\n",
    "< u, v > = u^t C v,\n",
    "$\n",
    ">  where $C$ is a matrix of size $n \\times n$.\n",
    ">\n",
    ">  The requirement that $<u,v>$ must be positive definite places restrictions on $C$ \n",
    ">\n",
    ">\n",
    "> * **Norm:**   $\\quad\\; \\lVert{u}\\rVert = \\sqrt{\\ u^t C u } $<br><br>\n",
    "> * **Metric:** $\\quad d(u,v) = \\lVert v - u \\rVert. $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2  Inner Products in Function Spaces"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Functions are Vectors:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> We can think of a vector $u \\in \\mathbb{R}^n$ as a **function that maps an index $i = 1,2, \\dots n$ to an entry $u_i$ of $u$.**\n",
    ">\n",
    "> We can generalize this to **a function $f$ that maps an index $x$ in some domain to a value $f(x).$**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">  $$\\begin{align}\n",
    "f(x)   &= x^2 + 1,\\quad x \\in \\left[ -1, 3 \\right],                  & \\text{ a vector in } \\mathscr{F}\\left[ -1, 3 \\right] \\quad\\quad\\quad\\quad \\\\\n",
    "f_s(x) &= x^2 + 1,\\quad x \\in \\left\\{ -1, 0, \\frac{1}{2}, 2 \\right\\},& \\text{ a vector in } \\mathbb{R}^4 :\\quad ( 2,\\ 1,\\ \\frac{5}{4},\\ 5 )\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "f(x) = x*x + 1\n",
    "\n",
    "x    = range(-1, stop=3, length=100);  y   = f.(x)\n",
    "x_s  = [ -1; 0; 1//2; 2];              y_s = f.(x_s)\n",
    "\n",
    "plot( x,   y,   label=\"f(x)=x^2+1\", color=:blue)\n",
    "plot!(x_s, y_s, t=:bar, bar_width=0.02, color=:green, label=:none)\n",
    "scatter!(x_s, y_s, label=\"f_s\",  legend=:topleft, yrange=(0,10.2), size=(300,200))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Example Inner Products:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> * $<f,g> = \\frac{1}{4}\\ \\int_{-1}^3{\\ f(x)\\ g(x)\\ dx }$\n",
    "> * $<f,g> = \\int_{-1}^3{\\ e^{(x+3)}\\ f(x)\\ g(x)\\ dx }.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> * **Norm:**   $\\quad \\lVert f \\rVert = \\sqrt{ <f,f> }, \\quad $ e.g., $\\quad \\lVert f \\rVert = \\sqrt{\\frac{1}{4} \\int_{-1}^3{\\ f^2(x)\\ dx}}$\n",
    "> * **Metric:** $\\quad d( f,g ) = \\lVert g-f \\rVert.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Orthogonality"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\quad\\quad u \\perp v \\quad \\Leftrightarrow \\quad < u,  v > = 0.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Example:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> $u = \\begin{pmatrix} u_1 \\\\ u_2 \\end{pmatrix}, \\; v = \\begin{pmatrix} v_1 \\\\ v_2 \\end{pmatrix}, \\quad < u , v > =  3\\ u_1\\ v_1\\ +\\ u_2\\ v_2 .$\n",
    "\n",
    "\n",
    "> Let $\\quad u = \\begin{pmatrix} 1 \\\\ 1 \\end{pmatrix}, \\;$ then\n",
    "$\\quad v \\perp u \\quad \\Leftrightarrow \\quad < v, u> = 0\\;$ requires $\\quad 3 v_1 + v_2 = 0$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot( [0; 1], [0; 1], arrow=2, label=\"u\", xrange=(-2,2),yrange=(0,3))\n",
    "plot!( [0; -1], [0; 3], arrow=2, label=\"v\", size=(300,200))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Gram Schmidt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Gram Schmidt works as before."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Consider $f_1(x) = 1, f_2(x)=x, f_3(x)=x^2,$<br><br>\n",
    "$\\quad\\quad$ the inner product $\\quad <f,g>\\ =\\ \\int_{-1}^1{\\ f(x)\\ g(x)\\ dx},$<br><br>\n",
    "\n",
    "$\\quad\\quad$ and the norm $\\quad\\quad\\;\\; \\lVert f \\rVert\\quad\\quad = \\ \\sqrt{ <f,f> }$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> We will need $\\quad\\quad\\frac{1}{2}  \\int_{-1}^{1}{\\ x^n\\ dx}\\ =\\quad \\left\\{ \n",
    "\\begin{align}\n",
    "    \\frac{1}{n+1} & \\quad &\\text{ if } n \\text{ is even}\\\\\n",
    "    0             &       & \\text{ if } n \\text{ is odd} \\;\n",
    "\\end{align}\n",
    "\\right.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Step 1**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> $$\\begin{align}\n",
    "w_1 &= f_1 \\Rightarrow w_1(x) = 1 \\quad\\quad\\quad\\quad\\; \\\\\n",
    "q_1 &= \\frac{\\sqrt{2}}{2}\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Step 2**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> $$\\begin{align}\n",
    " & w_2 & = \\quad& f_2 - \\frac{<f_2,w_1>}{<w_1,w_1>}\\ w_1 & \\quad\\quad\\quad\\\\\n",
    "&    & = \\quad& \\ x\\ -\\ \\frac{ <x,1>}{<1,1>}\\ 1 & \\\\\n",
    "&   & = \\quad& \\ x. & \\\\\n",
    "& q_2 & = \\quad& \\sqrt{\\frac{3}{2}}\\ x &  \\quad\\quad\\quad\\\\\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### **Step 3:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> $$\\begin{align}\n",
    " & w_3 & = \\quad& f_3 - \\frac{<f_3,w_1>}{<w_1,w_1>}\\ w_1 - \\frac{<f_3,w_2>}{<w_2,w_2>}\\ w_2 & \\quad\\quad\\quad\\\\\n",
    "&    & = \\quad& \\ x^2\\ -\\ \\frac{ <x^2,1>}{<1,1>}\\ 1  -\\ \\frac{ <x^2,x>}{<x,x>}\\ x & \\\\\n",
    "&   & = \\quad& \\ x^2 - \\frac{1}{3}. & \\\\\n",
    "& q_3 & = \\quad& \\sqrt{\\frac{45}{8}}\\ \\left( x^2 - \\frac{1}{3} \\right) &  \\quad\\quad\\quad\\\\\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4.Take Away"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.3",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.3"
  },
  "toc-autonumbering": false,
  "toc-showcode": false,
  "toc-showmarkdowntxt": false,
  "toc-showtags": false
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
