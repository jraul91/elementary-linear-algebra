{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import panel as pn\n",
    "pn.extension()\n",
    "\n",
    "import holoviews as hv\n",
    "hv.extension('bokeh', logo=False)\n",
    "\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\"><strong style=\"height:100px;color:darkred;font-size:40px;\">Basic Operations for Vectors and Matrices</strong></div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import YouTubeVideo\n",
    "YouTubeVideo(\"tFuLNVD0QRc\", 400, 200, frameborder=\"0\",\n",
    "      allow=\"accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture\",\n",
    "      allowfullscreen=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false,
    "toc-nb-collapsed": false
   },
   "source": [
    "# 1. Notation and Remarks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:** Model Vector and Matrix operations on the operations of scalars\n",
    "* The basic operations for scalars, i.e., **equality**, **addition, subtraction, multiplication** and **division**<br>\n",
    "are assumed known and will not be further defined. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Notation Reminder:** For simplicity in notation, we will write\n",
    "* $u = \\left( u_i \\right)_{i=1}^n$ for vectors in $\\mathbb{F}^n$ to mean $u = \\left( u_1, u_2, \\dots u_n \\right)$ $\\quad$ *Note:* an **ordered list** of scalars (one index)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* $A = \\left( a_{i j} \\right)_{i=1, j= 1}^{m,n}$ for matrices in $\\mathbb{F}^{m \\times n}$ to mean\n",
    "$A = \\begin{pmatrix} a_{1 1} & a_{1 2} & \\dots&  a_{1 n} \\\\\n",
    " a_{2 1} & a_{2 2} & \\dots & a_{2 n} \\\\\n",
    " \\dots & \\dots & \\dots &  \\dots \\\\\n",
    " a_{m 1} & a_{m 2} & \\dots&  a_{m n} \\\\\n",
    "\\end{pmatrix}$ $\\quad$ *Note:* a **rectangular** layout of scalars (two indices)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* we may simplify the notation and write $u=\\left( u_i \\right) \\in \\mathbb{F}^n$ and $A = \\left( a_{i j} \\right) \\in \\mathbb{F}^{m \\times n}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false,
    "toc-nb-collapsed": false
   },
   "source": [
    "# 2. Equality, Addition and Scalar Multiplication"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false,
    "toc-nb-collapsed": false
   },
   "source": [
    "## 2.1 Equality"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Let $u = \\left( u_i \\right)$ and $v = \\left( v_i \\right) $\n",
    "be two vectors in $\\mathbb{F}^n$.<br>\n",
    "$\\quad$ The vectors are **equal** iff\n",
    "$$\n",
    "u = v \\; \\Leftrightarrow \\; \\left\\{ \\begin{align} u_1 &= v_1 \\\\ u_2 &= v_2 \\\\ & \\dots \\\\ u_n & = v_n \\end{align} \\right.\n",
    "$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Let $A = \\left( a_{ i j} \\right)$ and $B = \\left( b_{i j} \\right) $\n",
    "be two matrices in $\\mathbb{F}^{m \\times n}$.<br>\n",
    "$\\quad$ The matrices are **equal** iff\n",
    "$$\n",
    "A = B \\; \\Leftrightarrow \\; a_{i j} = b_{i j} \\; \\text{ for each } i = 1,2, \\dots m \\; \\text{ and } j = 1, 2, \\dots n.\n",
    "$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks:**\n",
    "* the vectors and matrices on either side of an equal sign must be **the same size**\n",
    "* equality lets us convert **vector and matrix equations** into a **set of scalar equations** and back again.<br>\n",
    "We know the algebra of scalar equations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Example**\n",
    ">\n",
    "> $$\\begin{pmatrix} x_{1 1} & x_{1 2} \\\\ x_{2 1} & x_{2 2} \\\\ x_{3 1} & x_{3 2} \\end{pmatrix} = \\begin{pmatrix} 3 & \\sqrt{2} \\\\ 5 & \\pi \\\\ 9 & -1 \\end{pmatrix}\n",
    "\\; \\Leftrightarrow \\; \\left\\{\n",
    "\\begin{align}\n",
    "x_{1 1} &= 3 \\\\ x_{1 2} &= \\sqrt{2} \\\\\n",
    "x_{2 1} &= 5 \\\\ x_{2 2} &= \\pi \\\\\n",
    "x_{3 1} &= 9 \\\\ x_{3 2} &= -1\n",
    "\\end{align}\n",
    "\\right.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Addition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Let $u = \\left( u_i \\right)$ and $v = \\left( v_i \\right) $\n",
    "be two vectors in $\\mathbb{F}^n$.<br>\n",
    "$\\quad$ The **sum** of the vectors $u + v$ is defined as\n",
    "$$\n",
    "\\begin{pmatrix} u_{1} \\\\ u_{2} \\\\ \\dots \\\\ u_n \\end{pmatrix} \n",
    "+ \\begin{pmatrix} v_{1} \\\\ v_{2} \\\\ \\dots \\\\ v_n \\end{pmatrix} \\; = \\;\n",
    "\\begin{pmatrix} u_{1}+ v_1  \\\\ u_{2} + v_2 \\\\ \\dots \\\\ u_n + v_n \\end{pmatrix}\n",
    "$$\n",
    "i.e., $$ \\left( u + v \\right)_i = u_i + v_i \\; \\text{ for } i = 1, 2, \\dots n$$\n",
    "</div>   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Let $A = \\left( a_{ i j} \\right)$ and $B = \\left( b_{i j} \\right) $\n",
    "be two matrices in $\\mathbb{F}^{m \\times n}$.<br>\n",
    "$\\quad$ The **sum** of the matrices $A + B$ is defined as\n",
    "$$\n",
    "\\left( A + B \\right)_{i j}  \\; = \\;  a_{i j} + b_{i j} \\; \\text{ for } i = 1, 2, \\dots m, \\text{ and } j = 1, 2, \\dots m \n",
    "$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Example**\n",
    ">\n",
    "> $$\\begin{pmatrix} x_1 & y_1 & z_1 \\\\ x_2 & y_2 & z_2 \\end{pmatrix}\n",
    "+ \\begin{pmatrix} 1 & 2 & 3 \\\\ -1 & -2 & -3 \\end{pmatrix} \\; = \\;\n",
    "\\begin{pmatrix} x_1+1 & y_1+2 & z_1+3 \\\\ x_2 - 1 & y_2 -2 & z_2 -3\\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks:**\n",
    "* These definitions require the vectors and matrices to be the **same size!**<br>\n",
    "Addition is ***not defined*** otherwise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* <div style=\"background-color:#F2F5A9;color:black;\"> <strong>Theorem:</strong> Let $u = \\left( u_i \\right), v = \\left( u_i \\right), $ and $ c = \\left( c_i \\right)$ be vectors in $\\mathbb{F}^n$ with $u = v$. Then\n",
    "$$u = v \\; \\Leftrightarrow \\; u + \\color{red}c = v + \\color{red}c$$ </div>\n",
    "i.e., we can add or remove a vector on both sides of an equal sign.<br>\n",
    "The proof is simple: use the definition of equality!\n",
    "$$\n",
    "\\begin{align}\n",
    "\\left( \\xi \\right) & \\Leftrightarrow  u + c& = \\; & v + c & \\text{ named our equation } \\left( \\xi \\right) \\\\\n",
    "              & \\Leftrightarrow  u_i + c_i & = \\; & v_i + c_i \\; \\text{ for } i = 1, 2, \\dots n &  \\text{ definition of equality } \\\\\n",
    "              & \\Leftrightarrow  u_i       & = \\; & v_i       & \\text{ property of scalar equations } \\\\\n",
    "              & \\Leftrightarrow  u         & = \\; & v         & \\text{ definition of equality }\n",
    "\\end{align}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The same theorem holds for matrices $A, B, C$ and is proved in exactly the same way<br>\n",
    " <div style=\"background-color:#F2F5A9;color:black;\"> <strong>Theorem:</strong> Let $A, B, C$ be matrices in $\\mathbb{F}^{m \\times n}.$ Then\n",
    "    $$ A = B \\; \\Leftrightarrow A + \\color{red}C = B + \\color{red}C $$\n",
    " </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two similar theorems hold for multiplication by a scalar $\\alpha \\ne 0:$\n",
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "$$u = v \\Leftrightarrow \\color{red}\\alpha u = \\color{red}\\alpha v \\quad \\text{and} \\quad A = B \\; \\Leftrightarrow \\; \\color{red}\\alpha A = \\color{red}\\alpha B$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false,
    "toc-nb-collapsed": false
   },
   "source": [
    "## 2.3 Scalar Multiplication"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Given a vector $u = \\left( u_i \\right) \\in \\mathbb{F}^n$ and a scalar $\\alpha \\in \\mathbb{F}$,<br>\n",
    "    $\\quad$ the **scalar product** $\\alpha u$ is defined by\n",
    "$$\n",
    "\\left( \\alpha u \\right)_i = \\alpha \\; u_i \\; \\text{ for each } i = 1, 2, \\dots n\n",
    "$$ \n",
    "</div>\n",
    "Similarly,\n",
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Given a matrix $A = \\left( a_{i j} \\right) \\in \\mathbb{F}^{m \\times n}$ and a scalar $\\alpha \\in \\mathbb{F}^n$,<br>\n",
    "    $\\quad$ the **scalar product** $\\alpha A$ is defined by\n",
    "$$\n",
    "\\left( \\alpha A \\right)_{i j} = \\alpha \\; a_{i j}  \\; \\text{ for each } i = 1, 2, \\dots m, \\; j = 1, 2, \\dots n\n",
    "$$ \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Example:**\n",
    ">\n",
    "> $$\n",
    "3 \\begin{pmatrix} 2 & x \\\\ 1 & y \\\\ 3 & z \\end{pmatrix} \\; = \\;\n",
    "\\begin{pmatrix} 6 & 3 x \\\\ 3 & 3 y \\\\ 9 & 3 z \\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false,
    "toc-nb-collapsed": false
   },
   "source": [
    "## 2.4 Subtraction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Subtraction can now be defined as a combination of vector addition and scalar multiplication:\n",
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Given vectors $u$ and $v$  in $\\mathbb{F}^n,$ **subtraction** is defined by\n",
    "$$\n",
    "u - v = u + \\left( -1 v \\right)\n",
    "$$ \n",
    "</div>\n",
    "Similarly,\n",
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "\n",
    "**Definition:** Given matrices $A$ and $B$ in $\\mathbb{F}^{m \\times n},$ **subtraction** is defined by\n",
    "$$\n",
    "A - B = A + \\left( -1 B \\right)\n",
    "$$ \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Example:**\n",
    "$$\n",
    "\\begin{pmatrix} 1 & 2 & 3 \\\\ 3 & 2 & 1 \\end{pmatrix} - \\begin{pmatrix} 1 & 1 & 1 \\\\ 1 & 1 & 1 \\end{pmatrix} =\n",
    "\\begin{pmatrix} 0 & 1 & 2 \\\\ 2 & 1 & 0 \\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.5 System of Equations Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.5.1 Important Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{align}\n",
    "\\left( \\xi \\right)\n",
    "& \\Leftrightarrow \\; \n",
    "\\left\\{  \\begin{aligned}\n",
    "     \\color{darkred}{  3 x + 2 y + z}  &\\color{darkred}{  = 5 } \\\\\n",
    "     \\color{darkred}{  4 x - y + 2 z} &\\color{darkred}{  = 3 } \\\\\n",
    "\\end{aligned} \\right. & &  \\\\\n",
    "& & & \\\\\n",
    "& \\Leftrightarrow \\; \\begin{pmatrix} 3 x + 2 y + z \\\\ 4 x - y + 2 z \\end{pmatrix} \\; & = \\; \\begin{pmatrix} 5 \\\\ 3 \\end{pmatrix} & \\quad \\text{ by the definiton of equality} \\\\\n",
    "& \\Leftrightarrow \\; \\begin{pmatrix} 3 x \\\\ 4 x  \\end{pmatrix} + \\begin{pmatrix} 2 y \\\\  - y \\end{pmatrix} +  \\begin{pmatrix}  z \\\\  2 z \\end{pmatrix} \\;&  = \\; \\begin{pmatrix} 5 \\\\ 3 \\end{pmatrix} & \\quad \\text{ by the definiton of addition} \\\\\n",
    "& & & \\\\\n",
    "& \\Leftrightarrow \\;\\color{darkred}{  x \\begin{pmatrix} 3 \\\\  4 \\end{pmatrix}\n",
    "  + y \\begin{pmatrix} 2 \\\\ -1 \\end{pmatrix}\n",
    "  + z \\begin{pmatrix} 1 \\\\  2 \\end{pmatrix} } \\; & \\color{darkred}{  = \\; \\begin{pmatrix} 5 \\\\ 3 \\end{pmatrix}} & \\quad \\text{ by the definiton of scalar multiplication} \\\\\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks:**\n",
    "* initially the system is written in terms of equations that represent **planes**\n",
    "* the final representation is a **sum of suitably scaled vectors** adding up to a given righthand side"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.5.2 Two <strong>Very Important</strong> Definitions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;float:left;width:35%;height:3.5cm;\">\n",
    "<strong>Definition:</strong> Given a set of vectors $\\left\\{ v_1, v_2, \\dots v_k \\right\\}$ in $\\mathbb{F}^n$<br>\n",
    "$\\qquad$ and a set of scalars\n",
    "$\\left\\{ \\alpha_1, \\alpha_2, \\dots \\alpha_k \\right\\}$ in $\\mathbb{F}$,<br>\n",
    "$\\qquad$ An expression of the form\n",
    "$$\n",
    "\\color{red}{\\alpha_1 v_1 + \\alpha_2 v_2 + \\dots \\alpha_k v_k}\n",
    "$$\n",
    "$\\qquad$  is a <strong>linear combination of vectors</strong>\n",
    "</div>\n",
    "\n",
    "<div style=\"background-color:#F2F5A9;color:black;float:right;width:60%;height:3.5cm;\">\n",
    "<strong>Definition:</strong> Given a set of vectors $\\left\\{ v_1, v_2, \\dots v_k \\right\\}$ in $\\mathbb{F}^n$,<br>$\\qquad$\n",
    "    the <strong>span  $\\left\\{ v_1, v_2, \\dots v_k \\right\\}$</strong> is the set of all possible linear combinations of these vectors:\n",
    "<br><br>\n",
    "$$\n",
    "\\color{red}{ span \\left\\{ v_1, v_2, \\dots v_k \\right\\} = \\left\\{ w \\;\\mid\\; w = \\alpha_1 v_1 + \\alpha_2 v_2 + \\dots \\alpha_k v_k, \\text{ for all } \\alpha_1, \\alpha_2, \\dots \\alpha_k \\; \\text{ in } \\mathbb{F}\n",
    "\\right\\} }\n",
    "$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:** We can similarly define a **linear combination of matrices.**<br>\n",
    "$\\quad$ We will defer this to a letor part of the course (i.e., the discussion of Vector Spaces)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false
   },
   "source": [
    "# 3. Properties"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.1 Algebraic Properties"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;align:left;width:100%;\">\n",
    "<strong>Theorem:</strong> Given any vectors $u, v, w$ in $\\mathbb{F}^n$ and any scalars $\\alpha, \\beta$ in $\\mathbb{F}$\n",
    "<br><br>\n",
    "<div style=\"background-color:#F2F5A9;color:black;align:left;width:100%;height:2.5cm;\">\n",
    "<div style=\"float:left;width:49.%;background-color:#F2F5A9;color:black;padding-right:1cm;padding-left:3mm;\">\n",
    "$$\\begin{align}\n",
    "& u + v = v + u                                             & \\text{(commutativity)} \\\\\n",
    "& (u + v) + w = u + (v + w) \\quad                           & \\text{(associativity)} \\\\\n",
    "& \\exists 0 \\in \\mathbb{F}^n \\ni u + 0 = u                  & \\text{(identity element for vector addition)} \\\\\n",
    "& \\exists \\tilde{u}  \\in \\mathbb{F}^n \\ni u + \\tilde{u} = 0 & \\text{(existence of the additive inverse)}\\\\\n",
    "\\end{align}\n",
    "$$\n",
    "</div>\n",
    "<div style=\"float:left;width:52%;background-color:#F2F5A9;color:black;border-left: 2px solid black;\">\n",
    "$$\\begin{align}\n",
    "&    \\alpha \\left( u + v \\right) = (\\alpha u) + (\\alpha v) \\;\\;              &  \\text{(distributivity of scalar mult over vector addition)}\\\\\n",
    "&    \\left( \\alpha + \\beta \\right) v = (\\alpha v) + (\\beta v) \\;\\;          &  \\text{(distributivity of scalar mult over scalar addition)}\\\\\n",
    "&    \\alpha \\left( \\beta u \\right) = \\left( \\alpha \\beta \\right) u \\;\\; & \\text{(compatibility of addition and scalar mult)}\\\\\n",
    "&    1 v = v                                                             & \\text{(identity element for scalar mult)} \\\\\n",
    "\\end{align}\n",
    "$$\n",
    "</div></div>    \n",
    "<\\div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "<strong>Theorem:</strong>\n",
    "\n",
    "* The **zero element is unique**. It is $\\color{red}{0 = \\left( 0, 0, \\dots 0 \\right) }$\n",
    "    * The scalar product $\\color{red}{ \\quad 0\\ u = 0 } \\qquad$  (note the 0 on the left is a scalar, the 0 on the right is the zero vector)\n",
    "* The **additive inverse is unique**.\n",
    "    * The additive inverse of a vector $u$ is $\\color{red}{ \\quad \\tilde{u} = -1 u}$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;\">\n",
    "    <strong>Theorems:</strong> The exact same theorems hold for <strong>matrices</strong> $u, v, w$ in $\\mathbb{F}^{m \\times n}$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Uniqueness of the Additive Inverse:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assume we have two additive inverses $\\tilde{u}_1$ and $\\tilde{u}_2$ of $u$. Thus\n",
    "$$\n",
    " u+\\tilde{u}_1 = 0 \\label{eq1}\\tag{1}\n",
    "$$\n",
    "$$\n",
    " u+\\tilde{u}_2 = 0 \\label{eq2}\\tag{2} \\\\\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then\n",
    "$$\\begin{align}\n",
    "\\tilde{u}_1 &= \\tilde{u}_1 + 0                   & \\text{identity element for vector addition}\\\\\n",
    "            &= \\tilde{u}_1 + ( u + \\tilde{u}_2 ) & \\text{by Eq 2}\\\\\n",
    "            &= ( \\tilde{u}_1 + u ) + \\tilde{u}_2 \\qquad \\qquad & \\text{associativity}\\\\\n",
    "            &=  0 + \\tilde{u}_2                  & \\text{by Eq 1}\\\\\n",
    "            &= \\tilde{u}_2                       & \\text{identity element for vector addition} \\\\\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "Simplifying the notation\n",
    "\n",
    "**Remark:** We can omit parentheses and write\n",
    "* $u + (v + w) = (u+v) + w = u + v + w$\n",
    "* $(\\alpha u)+(\\beta v) = \\alpha u + \\beta v \\quad$ (scalar multiplication has **precedence** over vector addition)\n",
    "\n",
    "**Remark:**\n",
    "* The additive inverse is $-1 u = - u$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Solve the following vector equation in $u,v$ and $x$ for the vector $x$:\n",
    "$$\\begin{align}\n",
    "(\\xi) & \\; \\Leftrightarrow \\; & 3 u +2 v - 6 x & = 3 x - v & \\\\\n",
    "      & \\; \\Leftrightarrow \\; & 3 u +2 v - 6 x + ( -3 u - 2 v - 3 x ) & = 2 x - v + (-3 u - 2 v - 2 x ) & \\text{ adding the same vector on both sides} \\\\\n",
    "      & \\; \\Leftrightarrow \\; & - 9 x & = -3 u - 3 v& \\text{ simplifying using the algebraic properties}\\\\\n",
    "      & \\; \\Leftrightarrow \\; & x & = \\frac{1}{3} u + \\frac{1}{3} v& \\text{ multiplication by } -\\frac{1}{9} \\text{ and simplification}\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.2 Geometric Representation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2.1 Vector Addition and Subtraction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When using arrows, addition and subtraction of two vectors can be represented by the diagonals of a parallelogram: "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;\"><img src=\"Figs/VectorSumDiff.svg\" width=350></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:** Parallelogram constructions are **flat:**<br>$\\qquad$ adding two vectors $u$ and $v$ results in a vector **in the plane** defined by $u$ and $v$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2.2 Linear Combinations of Vectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Example: the set of points $p = \\alpha \\left( 1, 2 \\right)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot(u, start=[0,0]):\n",
    "    us = u + start\n",
    "    return hv.Curve(((start[0],us[0]),[start[1],us[1]])) * hv.Scatter((us[0],us[1]))\n",
    "\n",
    "u   = np.array([1,2])\n",
    "h_u = plot(u).opts( hv.opts.Curve(xticks=10,yticks=10,xlim=(-5,5),ylim=(-10,10),color='red',line_width=3, width=600),\n",
    "                    hv.opts.Scatter( size=5,color='red'))\n",
    "pn.interact( lambda alpha : ( plot(alpha * u).opts( hv.opts.Curve(color='blue',line_width=1),\n",
    "                                                    hv.opts.Scatter( size=8,color='blue')) * h_u) \\\n",
    "  .opts(title='Red: vector u;   Blue dot at α u', show_grid=True), alpha=(-4,4,.5) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rng(a,b,p=1):\n",
    "    l = np.linspace(a,b,(b-a)*p+1 )\n",
    "    return (l,2*l)\n",
    "h=(hv.Scatter( rng( 0,4,1) ).opts(size=8)*h_u).opts(title=\"alpha=[0,1,2,3,4]\",             show_grid=True, height=250,width=400)+\\\n",
    "  (hv.Scatter( rng( 0,4,4) ).opts(size=8)*h_u).opts(title=\"alpha=[0,0.25,0.50,...,4]\",     show_grid=True, height=250,width=400)+\\\n",
    "  (hv.Scatter( rng(-4,4,4) ).opts(size=8)*h_u).opts(title=\"alpha=[-4,...,0,0.25,...,4]\",   show_grid=True, height=250,width=400)+\\\n",
    "  (hv.Scatter( rng(-4,4,10)).opts(size=8)*h_u).opts(title=\"alpha=[-4,...,0,0.1,0.2,...,4]\",show_grid=True, height=250,width=400)\n",
    "h.cols(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Example: the set of points $p = \\alpha \\left( 1, 0 \\right) + \\beta \\left( 1, 4 \\right)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u=np.array([1,0])\n",
    "v=np.array([1,4])\n",
    "\n",
    "h_au = hv.Scatter( ([0,0]), label='alpha u' ).opts( alpha=1, size=0, legend_position='right', color=\"blue\")*\\\n",
    "       hv.Scatter( ([0,0]), label='beta v'  ).opts(color='red', size=0)\n",
    "l1=[];l2=[]\n",
    "\n",
    "def plot2(alpha,beta):\n",
    "    au = alpha*u; bv=beta*v; p=au+bv; l1.append(p[0]); l2.append(p[1])\n",
    "    h = plot(au    ).opts(hv.opts.Curve(color='blue'), hv.opts.Scatter(color='blue', size=10)) *\\\n",
    "        plot(bv, au).opts(hv.opts.Curve(color='red' ), hv.opts.Scatter(color='red',  size=10)) *\\\n",
    "        hv.Scatter((l1,l2)                          ).opts(color='darkgreen',alpha=.3)*\\\n",
    "        h_au\n",
    "\n",
    "    h = h.opts( hv.opts.Curve(xticks=10,yticks=10,xlim=(-10,10),ylim=(-15,15),line_width=3, width=800, show_grid=True))\n",
    "    return h.opts(title=\"alpha u + beta v\")\n",
    "\n",
    "pn.interact( plot2, alpha=(-5.,5.,.25), beta=(-3.,3.,.25))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Conclusion:**<br>$\\qquad$ **spans** of vectors are **hyperplanes**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4 Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* two important definitions: a **linear combination** and a **span of vectors**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* the **algebra of vector and matrix addition and scalar multiplication** works similarly to the addition and multiplication of scalars\n",
    "    * set of equations of hyperplanes can be transformed into an equation involving a linear combination of vectors\n",
    "    * the scalars 0, 1 and -1 are special: given a vector $u,$ the scalar products\n",
    "    $$\\begin{align}\n",
    "    0 u &= 0 \\\\\n",
    "    1 u &= u \\\\\n",
    "    -1 u &= -u \\; \\text{ (the additive inverse) }\n",
    "    \\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* a span of vectors can be thought of as a **hyperplane**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
