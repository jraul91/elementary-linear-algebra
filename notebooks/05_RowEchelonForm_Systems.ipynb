{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "import panel as pn; import holoviews as hv\n",
    "pn.extension();     hv.extension('bokeh', logo=None)\n",
    "import numpy as np\n",
    "\n",
    "from IPython.core.interactiveshell import InteractiveShell\n",
    "InteractiveShell.ast_node_interactivity = \"all\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align:center;\"><strong style=\"height:100px;color:darkred;font-size:40px;\">Systems of Linear Equations (Part I)</strong><br>\n",
    "    <strong style=\"height:100px;color:darkred;font-size:30px;\">Row Echelon Form and Backsubstitution</strong>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import YouTubeVideo\n",
    "YouTubeVideo(\"rH0EdQRDO1A\", 400, 200, frameborder=\"0\",\n",
    "      allow=\"accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture\",\n",
    "      allowfullscreen=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Systems of Linear Equations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 Definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;background-color:#F2F5A9;color:black;width:48%;height:6.5cm;\">\n",
    "<strong>Definition:</strong> A <strong>system of $m$ linear equation in $n$ unknowns</strong><br> (equation form)\n",
    "is a set of equations of the form\n",
    "$$\n",
    "\\begin{align}\n",
    "a_{1 1} x_1 + a_{1 2} x_2 + \\dots a_{1 n} x_n &= b_1 \\\\\n",
    "a_{2 1} x_1 + a_{2 2} x_2 + \\dots a_{2 n} x_n &= b_2 \\\\\n",
    "\\dots \\quad & \\;\\; \\dots \\\\\n",
    "a_{m 1} x_1 + a_{m 2} x_2 + \\dots a_{m n} x_n &= b_m \\\\\n",
    "\\end{align}\n",
    "$$\n",
    "<br>\n",
    "the <strong>coefficients</strong> $a_{i j}$ and the <strong>right hand sides</strong> $b_i$ are known scalars in $\\mathbb{F}$.<br>\n",
    "The variables $x_i$ are <strong>unknown</strong>.\n",
    "</div>\n",
    "<div style=\"float:right;background-color:#F2F5A9;color:black;width:51%;height:6.5cm;\">\n",
    "<strong>Definition:</strong> A <strong>system of $m$ linear equation in $n$ unknowns</strong> is a matrix equation of the form\n",
    "$$\\color{darkred}{ A x = b}, $$\n",
    "where the <strong> coefficient matrix</strong> $A \\in \\mathbb{F}^{m \\times n}$<br>\n",
    "and the <strong>right hand side vector</strong> $b \\in \\mathbb{F}^m$ are given,<br>\n",
    "and $x \\in \\mathbb{F}^n$ is the <strong>vector of unknowns:</strong>\n",
    "$$\n",
    "A = \\begin{pmatrix}\n",
    "a_{1 1} & a_{1 2} & \\dots & a_{1 n} \\\\\n",
    "a_{2 1} & a_{2 2} & \\dots & a_{2 n} \\\\\n",
    "\\dots   & \\dots   & \\dots & \\dots   \\\\\n",
    "a_{m 1} & a_{m 2} & \\dots & a_{m n} \\\\\n",
    "\\end{pmatrix}, \\quad\n",
    "b = \\begin{pmatrix} b_{1} \\\\ b_{2} \\\\ \\dots \\\\ b_{m} \\\\\\end{pmatrix}, \\quad\n",
    "x =\\begin{pmatrix} x_{1} \\\\ x_{2} \\\\ \\dots \\\\ x_{n} \\\\\\end{pmatrix}\n",
    "$$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Examples**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:48%;height:5.5cm;\">\n",
    "\n",
    "| Equations | Comment |\n",
    "| ---- | ---- |\n",
    "| $$3 x = 5 \\quad$$ |  a system of 1 equation in 1 unknown |\n",
    "| $$\\left\\{ \\begin{align} 3 x &= 5 \\\\ 6 x &= 10 \\end{align} \\right.  \\quad$$ | a system of 2 equations in 1 unknown |\n",
    "| $$\\left\\{ \\begin{align} 3 x + 2 y - z &= 4 \\\\ 6 x - 5 y &= 10 \\end{align} \\right.  \\quad$$ | a system of 2 equations in 3 unknowns |\n",
    "| $$\\left\\{ \\begin{align} 3 x + 2 y - z &= 5 \\\\ 6 \\sin (x) + y &= 10 \\end{align} \\right.  \\quad$$ | a nonlinear system |\n",
    "</div>\n",
    "<div style=\"float:left;width:48%;height:5.5cm;\">\n",
    "\n",
    "| Equations | Comment |\n",
    "| ---- | ---- |\n",
    "| $$ ( 3 ) ( x ) = ( 5 ) \\quad$$ |  a system of 1 equation in 1 unknown |\n",
    "| $$ \\begin{pmatrix} 3 \\\\ 6  \\end{pmatrix}\\; ( x ) = \\begin{pmatrix} 5 \\\\ 10 \\end{pmatrix} \\quad$$ | a system of 2 equations in 1 unknown |\n",
    "| $$ \\begin{pmatrix} 3 & 2 & -1 \\\\ 6 & -5 & 0 \\end{pmatrix}\\;  \\begin{pmatrix} x \\\\ y \\\\ z \\end{pmatrix}\\; = \\; \\begin{pmatrix} 4 \\\\ 10 \\end{pmatrix} \\quad$$ | $$\\text{a system of 2 equations in 3 unknowns}$$ |\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks:**\n",
    "* partitioning the $A$ matrix into rows yields the **row view** of the system\n",
    "$$ \\left( \\begin{matrix} 3 & 2 & -1 \\\\ \\hline 6 & -5 & 0 \\end{matrix} \\right) \\; \n",
    "   \\begin{pmatrix} x \\\\ y \\\\ z \\end{pmatrix}\\;\n",
    "   = \\; \\left( \\begin{matrix} 4 \\\\ \\hline 10 \\end{matrix} \\right)\n",
    "   \\; \\Leftrightarrow \\; \\left\\{   \\begin{align} 3 x + 2 y - z &= 4 \\\\ 6 x - 5 y &= 10 \\end{align} \\right.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* partitioning the $A$ matrix into columns yields the **column view** of the system\n",
    "$$ \\left( \\begin{array}{r|r|r} 3 & 2 & -1 \\\\6 & -5 & 0 \\end{array} \\right) \\; \n",
    "   \\begin{pmatrix} x \\\\ \\hline y \\\\ \\hline z \\end{pmatrix}\\;\n",
    "   = \\; \\left( \\begin{matrix} 4 \\\\ 10 \\end{matrix} \\right)\n",
    "   \\; \\Leftrightarrow \\; x \\begin{pmatrix} 3 \\\\ 6 \\end{pmatrix} +  y \\begin{pmatrix} 2 \\\\ -5 \\end{pmatrix} + z  \\begin{pmatrix} -1 \\\\ 0 \\end{pmatrix} =  \\begin{pmatrix} 4 \\\\ 10 \\end{pmatrix} \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 Solutions of a System of Linear Equations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;background-color:#F2F5A9;color:black;width:80%;\">\n",
    "<strong>Definition:</strong> A <strong>solution</strong> to a linear system of equations $A x = b$ is an assignment of values to the variable $x$<br>\n",
    "    $\\qquad$ such that all the equations are simultaneously satisfied"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the following three systems of equations:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:27%;height:4cm;border:2px solid black;\">\n",
    "$\\;(\\xi_1) \\Leftrightarrow \\left\\{ \\begin{align} 3 x + 2 y &= 7 \\\\ 2 x - 3 y &= -4 \\end{align} \\right. $<br>\n",
    "$\\;$which describes the intersection of two lines.<br><br>\n",
    "$\\;$The solution is <strong>unique.</strong>\n",
    "</div>\n",
    "<div style=\"float:left;width:30%;height:4cm;border-top:2px solid black;border-bottom:2px solid black;\">\n",
    "$\\;(\\xi_2) \\Leftrightarrow \\left\\{ \\begin{align} 3 x + 2 y &= 7 \\\\ 6 x + 4 y &= 14 \\end{align} \\right. $<br>\n",
    "$\\;$which describes two overlaid lines.<br><br>\n",
    "$\\;$The solution consists of the set of points on the line<br>\n",
    "$\\;$There are an <strong>infinite number</strong> of solutions.\n",
    "</div>\n",
    "<div style=\"float:left;width:30%;height:4cm;border:2px solid black;\">\n",
    "$\\;(\\xi_3) \\Leftrightarrow \\left\\{ \\begin{align} 3 x + 2 y &= 7 \\\\ 6 x + 4 y &= 5 \\end{align} \\right. $<br>\n",
    "$\\;$which describes two parallel lines.<br><br>\n",
    "$\\;$The solution consists of the set of points on the line<br>\n",
    "$\\;$There are an <strong>infinite number</strong> of solutions.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A=np.array( [[3, 2], [2, -3]] ); b = np.array([7,-4])\n",
    "print()\n",
    "print( \"(ξ_1) has solution: \", np.linalg.solve(A, b))\n",
    "\n",
    "h1 = hv.Curve(([7.0/3, 0], [0, 7.0/2]), label='L1' )*hv.Curve(([-2,2], [0, 8.0/3]), label='L2' )\n",
    "h2 = hv.Curve(([7.0/3, 0], [0, 7.0/2]), label='L1'  )*hv.Curve(([7.0/3, 0], [0, 7.0/2]), label='L2' ).opts(line_width=3,muted_alpha=0)\n",
    "h3 = hv.Curve(([7.0/3, 0], [0, 7.0/2]), label='L1'  )*hv.Curve(([-1.5, 1], [(5.+6.*1.5)/4, (5.-6.)/4]), label='L2'  )\n",
    "\n",
    "h1.opts(title=\"(ξ_1) Unique Solution\",width=250,height=250, legend_position='bottom', show_grid=True)+\\\n",
    "h2.opts(title=\"(ξ_2) ∞ Number of Solutions\",width=250,height=250, legend_position='bottom', show_grid=True)+\\\n",
    "h3.opts(title=\"(ξ_3) No Solution\",width=250,height=250, legend_position='bottom', show_grid=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks:**\n",
    "* The solution algorithm we will develop will show these numbers of solutions to be the only possible:<br>\n",
    "$\\qquad$ **no solution**, **a unique solution**, or **an infinite number of solutions**\n",
    "* We therefore need to talk about **sets of solutions** for linear systems of equations $A x = b$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Solutions of Row Echelon Form Systems"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1 Two Simple Examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example 1**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the system $(\\xi_1) \\Leftrightarrow \\left\\{ \\begin{align} x+ 3 y & = 5 \\\\ 2 y & = 2 \\end{align} \\right.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> We notice it is easy to solve \"from the bottom up\"  (**\"backwards\"**):\n",
    ">\n",
    "> * Start with the last equation $2 y = 2 \\quad \\Leftrightarrow y = 1$\n",
    "> * Now substitute the solution $y = 1$ into the previous equation:<br>\n",
    "> $\\qquad\\qquad\\qquad\\qquad$ $ x + 3 y \\; \\big|_{\\ y=1} = 5 \\quad \\Leftrightarrow \\; x = 5-3 = 2$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> We have obtained the **unique solution** $\\begin{pmatrix} x \\\\ y \\end{pmatrix} = \\begin{pmatrix} 2 \\\\ 1 \\end{pmatrix}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example 2**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the system $(\\xi_2) \\Leftrightarrow \\left\\{ \\begin{align} x + 3 y - z & = 6 \\\\ 2 y - z & = 2 \\\\ 4 z &= 16 \\end{align} \\right.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> We notice it is again easy to solve \"from the bottom up\"  (**\"backwards\"**):\n",
    ">\n",
    "> * Start with the last equation $4 z = 16 \\quad \\Leftrightarrow z = 4$\n",
    "> * Now substitute the solution $z = 4$ into the previous equation:<br>\n",
    "> $\\qquad\\qquad\\qquad\\qquad$ $ 2 y - z \\; \\big|_{\\ {z=4}} = 2 \\quad \\Leftrightarrow \\; y = \\frac{1}{2} ( 2 + 4 ) = 3$\n",
    "> * Finally substitute the solutions $y=3, z=4$ into the first equation:<br>\n",
    "> $\\qquad\\qquad\\qquad\\qquad$ $ x + 3 y - z \\; \\big|_{\\ {y=3 \\\\ z=4}} = 6 \\quad \\Leftrightarrow \\; x = ( 6 - 3 \\times 3 + 4 ) = 1$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> We have obtained the **unique solution** $\\begin{pmatrix} x \\\\ y \\\\ z \\end{pmatrix} = \\begin{pmatrix} 1 \\\\ 3 \\\\ 4 \\end{pmatrix}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **What Makes this Work?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looking at these two examples, we see that\n",
    "* the first variable (i.e., the **leading** variable is different in each case\n",
    "    * the first variable $x$ is the leading variable in the first equation\n",
    "    * the second variable $y$ is the leading variable in the second equation\n",
    "    * the third variable $z$ is the leading variable in the third equation\n",
    "    * $\\dots$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* as long as we have an equation for each of the variables,<br>\n",
    "  **a system satisfying this pattern can easily be solved from the bottom up!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Systems that do NOT have an Equation for Each of the Variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example 1**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider $(\\xi_1) \\Leftrightarrow x + 2 y - z = 1$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> There are three variables, but only one constraint. If we **choose $y$ and $z$ to be any values**, we can solve for the leading variable $x$!\n",
    ">\n",
    "> Let $y = \\alpha$, $z = \\beta$ where $\\alpha$ and $\\beta$ are parameters allowed to take on any value, then<br>$\\qquad$ $x = 1 -2 y + z \\ \\bigg|_{y=\\alpha,\\; z=\\beta} = 1 - 2 \\alpha + \\beta$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> By convention, we will write solutions in vector notation. This is known as the **standard form** of the solution:\n",
    ">\n",
    "> $$\\left. \\begin{align} x &= 1 - 2 \\alpha + \\beta \\\\ y &= \\alpha \\\\ z &= \\beta \\end{align} \\right\\}\\; \\Leftrightarrow \\;\n",
    "\\begin{pmatrix} x\\\\ y \\\\ z \\end{pmatrix} = \\begin{pmatrix} 1 \\\\ 0 \\\\ 0 \\end{pmatrix} + \\alpha  \\begin{pmatrix} -2 \\\\ 1 \\\\ 0 \\end{pmatrix} + \\beta \\begin{pmatrix} 1 \\\\ 0 \\\\ 1 \\end{pmatrix} $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Remark:** The original equation is readily recognized as a plane with a normal vector $(1, 2, -1)$.<br>\n",
    "> $\\qquad$ The solution represents the same plane as **a linear combination of two vectors** in the plane<br>\n",
    "> $\\qquad$ **pushed away from the origin** by the vector $(1, 0, 0)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important Remarks:**\n",
    "* $\\qquad$ We could have rewritten the equation with a **different leading variable**, i.e., $(\\xi_1) \\Leftrightarrow -z + x + 2 y = 1$<br>\n",
    "$\\qquad$ The solution then becomes\n",
    "$$\n",
    "\\begin{pmatrix} x\\\\ y \\\\ z \\end{pmatrix} = \\begin{pmatrix} 0 \\\\ 0 \\\\ -1 \\end{pmatrix} + \\alpha  \\begin{pmatrix} 1 \\\\ 0 \\\\ 1 \\end{pmatrix} + \\beta \\begin{pmatrix} 0 \\\\ 1 \\\\ 2 \\end{pmatrix} $$\n",
    "* The solution may look different, but it describes **the same plane** pushed away from the origin!\n",
    "* The **number of variables** for which we do not have constraints **does not change!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example 2**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the system $$(\\xi_2) \\Leftrightarrow \\left\\{ \\begin{align} -2 x_1 & + x_2 - x_3 + x_4 &= 0 \\\\ & - x_2 + x_3 + 2 x_4 &= 1 \\end{align} \\right.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The leading variables are $x_1$ and $x_2$.<br>\n",
    "> $\\qquad$ We are free to choose any values for the remaining variables,<br>\n",
    "> $\\qquad$ and write down the corresponding solution for the leading variables.<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Let $x_3 = \\alpha, x_4 = \\beta,$ where $\\alpha$ and $\\beta$ are parameters allowed to take on any value.by \n",
    "> Solving backwards, we get\n",
    "> $$\\begin{align}x_2 &= -\\left( 1 - x_3 - 2 x_4 \\right) \\bigg|_{x_3=\\alpha,\\; x_4=\\beta} \\\\ &= -1 + \\alpha + 2 \\beta \\end{align}$$\n",
    ">\n",
    "> and\n",
    "> $$\\qquad\\qquad\\qquad  \\begin{align}\n",
    "x_1 &= -\\frac{1}{2} \\left( 0 - x_2 + x_3 - x_4 \\right) \\ \\bigg|_{x_2 = -1 + \\alpha + 2 \\beta,\\; x_3 = \\alpha, x_4 = \\beta} \\\\\n",
    "    &= \\frac{1}{2}+\\frac{3}{2} \\beta\n",
    "\\end{align}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> In standard form, we see that\n",
    "> $$\n",
    "\\begin{pmatrix} x_1 \\\\ x_2 \\\\ x_3 \\\\ x_4 \\end{pmatrix} = \\begin{pmatrix} \\frac{1}{2} \\\\ -1 \\\\ 0 \\\\ 0 \\end{pmatrix} + \\alpha \\begin{pmatrix} 0 \\\\ 1 \\\\ 1 \\\\ 0 \\end{pmatrix} + \\beta \\begin{pmatrix} \\frac{3}{2} \\\\ 2  \\\\ 0 \\\\ 1 \\end{pmatrix} \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Note that we could have **rearranged** the problem to make $x_3$ or $x_4$ **the leading variable** in the second equation.<br>\n",
    "> <div><strong style=\"height:100px;color:darkred;font-size:20px;\">Try It! What does the standard form look like?</strong></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. The Backsubstitution Algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us look at these simple systems in matrix form, and introduce some vocabulary."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example 1**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the system from the previous section, and rewrite it in matrix form, with the leading variables shown in a box:\n",
    "$$\\begin{aligned}\n",
    "(\\xi_1) & \\Leftrightarrow \\left\\{ \\begin{align} -2 x_1 & + x_2 - x_3 +& x_4 &= 0 \\\\\n",
    "                                                       & - x_2 + x_3 +& 2 x_4 &= 1\\\\\n",
    "                                                       &              & 3 x_4 &= 5 \\end{align} \\right. \\\\\n",
    "        & \\Leftrightarrow \\begin{pmatrix} \\boxed{\\color{darkred}{-2}} & 1 & -1 & 4 \\\\\n",
    "                                           0            &\\boxed{ \\color{darkred}{-1}} & 1 & 2 \\\\\n",
    "         0 & 0 & 0 & \\boxed{ \\color{darkred}{4}}\\end{pmatrix}\n",
    "                          \\begin{pmatrix} x_1 \\\\ x_2 \\\\ x_3 \\\\ x_4 \\end{pmatrix} = \\begin{pmatrix} 0 \\\\ 1 \\\\ 5\\end{pmatrix}\n",
    "\\end{aligned}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:** We need to **recognize the simple systems** we have been discussing when they are **in matrix form!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1.1 Definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We really should talk about relative index positions on what follows. For convenience\n",
    "* a row **above** a given row or a **previous row** to a given row will mean a row with a lower row index than the row index of the given row.\n",
    "* a column to the **left** of a given column will mean a colum with a lower column index than the column index of the given column.\n",
    "* a row **below** a given row, and a column to the **left** of a given row will be similarly defined."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;background-color:#F2F5A9;color:black;width:100%;\">\n",
    "\n",
    "**Definition:** A matrix is in **row echelon form** iff\n",
    "* the leading entry (i.e., the first non-zero entry) in any given row is to the right of the leading entry in any row above the given row\n",
    "* no row with a leading entry appears below a row with all zero entries.\n",
    "    \n",
    "**Definition:** the leading entries are **pivots**, the **pivot row** and **pivot columns** are the row and column indices\n",
    "\n",
    "**Definition:** variables that do not have pivots are **free variables.**<br>\n",
    "    $\\qquad\\quad\\;\\;$ variables that do have pivots are **basic variables.**\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"Figs/rowechelon_form.svg\" style=\"margin:0px 0px 0cm 2cm;width:20cm;\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Augmented Form**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;background-color:#F2F5A9;color:black;width:100%;\">\n",
    "\n",
    "**Definition:** The **augmented form** of a system of linear equations $A x = b$ is the matrix $\\left( A \\ \\big| \\ b \\right)$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;width:50%;font-size:18px;margin:10px 10px 0cm 0cm;\"> Matrix Multiplication   A x = b <img src=\"Figs/augmented_matrix_1.svg\" style=\"margin:0px 0px 0cm 4cm;width:70%\"></div>\n",
    "<div style=\"float:left;font-size:18px;border:2px solid black;padding:5px;\"> <strong>Notation Convention</strong> for hand computations<br><br><img src=\"Figs/augmented_matrix_2.svg\" style=\"margin:0px 10px 0.5cm 0.5cm;width:11cm;\"></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **The Algorithm**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;background-color:#F2F5A9;color:black;width:100%;\">\n",
    "\n",
    "**Backsubstitution Algorithm:** Given a system in row echelon form\n",
    "* assign different parameters for each of the free variables (if any)\n",
    "* transcribe the equations and solve starting at the last equation and working backwards\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### **Example 2**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;font-size:18px;width:40%;\"> Consider the following system <br> <img src=\"Figs/augmented_matrix_3.svg\" style=\"margin:0px 0px 0cm 3cm;width:10cm;\"></div>\n",
    "\n",
    "<div style=\"float:left;font-size:15px;margin:0cm 0cm 0cm 2cm;\">\n",
    "\n",
    "> <strong>Step 1: </strong> Make sure the system is in row echelon form<br>\n",
    "<strong>Step 2: </strong> Assign parameters to each of the free variables<br> \n",
    "<strong>Step 3: </strong> Transcribe the equations and solve from the bottom up<br>\n",
    "<strong>Step 4: </strong> Write the solution in standard form\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Step 3:** solve each equation in turn from last to first\n",
    ">$$\\left.\n",
    "\\begin{align}\n",
    "{\n",
    "\\left.\n",
    "\\begin{aligned} \\boxed{ x_2 = \\alpha} & \\\\\n",
    "              \\boxed{x_4 = \\beta} & \\\\\n",
    "              x_3 = \\frac{1}{2} \\left( -1 - x_4 \\right) & \\end{aligned}\n",
    "\\right\\}  \\; \\Rightarrow \\; \\qquad \\boxed{x_3 = - \\frac{1}{2} -\\frac{1}{2} \\beta}\n",
    "} & \\\\\n",
    "& \\\\\n",
    " x_1 = 5 - 2 x_2 - x_3 - x_4 &\n",
    "\\end{align}\n",
    "\\right\\} \\; \\Rightarrow \\;\n",
    "\\boxed{x_1 = \\frac{11}{2} - 2 \\alpha  -\\frac{1}{2} \\beta }\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Step 4:** Write the solution in standard form\n",
    "> $$ \\begin{pmatrix} x_1 \\\\ x_2 \\\\ x_3 \\\\ x_4 \\end{pmatrix} \\; = \\;\n",
    "\\frac{1}{2}\\begin{pmatrix}11 \\\\ 0 \\\\ -1 \\\\ 0 \\end{pmatrix} \\; + \\;\n",
    "\\alpha\\ \\frac{1}{2}\\begin{pmatrix} -4 \\\\ 2 \\\\ 0 \\\\ 0\\end{pmatrix}  \\; + \\;\n",
    "\\beta\\ \\frac{1}{2}\\begin{pmatrix} -1 \\\\ 0 \\\\ -1 \\\\ 2 \\end{pmatrix} \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks:**\n",
    "* the geometric representation of the solution is **a hyperplane pushed away from the origin** by a constant vector\n",
    "* **Any choice of $\\alpha, \\beta$ is a valid solution**, in particular, the choice $x_2 = \\alpha=0,\\; x_4=\\beta=0$ is the constant vector in the solution above"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* the **form of the solution** is not unique: e.g.,<br> setting $\\alpha = 1 + \\tilde{\\alpha}, \\; \\beta = \\tilde{\\alpha} + \\tilde{\\beta}$ yields a solution **representing the same plane**<br><br>\n",
    "$$ \\begin{pmatrix} x_1 \\\\ x_2 \\\\ x_3 \\\\ x_4 \\end{pmatrix} \\; = \\;\n",
    "\\frac{1}{2}\\begin{pmatrix}7 \\\\ 2 \\\\ -1 \\\\ 0 \\end{pmatrix} \\; + \\;\n",
    "\\tilde{\\alpha}\\ \\frac{1}{2}\\begin{pmatrix} -5 \\\\ 2 \\\\ -1 \\\\ 2\\end{pmatrix}  \\; + \\;\n",
    "\\tilde{\\beta}\\ \\frac{1}{2}\\begin{pmatrix} -1 \\\\ 0 \\\\ -1 \\\\ 2 \\end{pmatrix} \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* **check the solution** by substituting each of the vectors:\n",
    "    * the constant solution must satisfy $A x = b$\n",
    "    * as we shall see later, each of the other vectors must satisfy $A x = 0$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Important concepts: **augmented matrix,** **row echelon form**, **pivot,**  **basic and free variables**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Systems in **row echelon form** are easy to solve by the **backsubstitution algorithm**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The geometric representation of **the solution is a hyperplane** pushed away from the origin by a constant vector"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
