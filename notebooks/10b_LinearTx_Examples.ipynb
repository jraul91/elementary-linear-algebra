{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "using LinearAlgebra, RowEchelon, LaTeXStrings, Plots\n",
    "include(\"LAcodes.jl\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\">\n",
    "    <strong style=\"height:60px;color:darkred;font-size:40px;\">The Matrix of a Linear Transformation</strong><br>\n",
    "    <strong style=\"height:40px;color:darkred;font-size:30px;\">Mapping a Vector Space into a Vector Space</strong>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "<div style=\"float:left;background-color:#F2F5A9;color:black;padding:2mm 2mm 2mm 2mm;height:4cm;width:46%;\">\n",
    "\n",
    "**Theorem:** Let $T: \\mathbb{F}^N \\rightarrow \\mathbb{F}^M$ be a linear transformation,<br>\n",
    "$\\qquad$ and let $e_i$ be the $i^{th}$ column of $I_N$.<br>\n",
    "$\\qquad$ Then $\\quad$ $T x = A_T x,$<br><br>\n",
    "$\\qquad$ where $A_T$ is given by\n",
    "    $$A_T = \\left( T e_1 \\mid T e_2 \\mid \\dots \\mid T e_N \\right)$$\n",
    "</div>\n",
    "<div style = \"float:left;padding-left:1cm;\">\n",
    "\n",
    "**Example:**<br>\n",
    "$$\n",
    "\\left.\n",
    "\\begin{align}\n",
    "\\begin{pmatrix} 1\\\\0\\end{pmatrix} & \\xrightarrow{T} \\begin{pmatrix}2\\\\3\\end{pmatrix} \\\\\n",
    "\\begin{pmatrix} 0\\\\1\\end{pmatrix} & \\xrightarrow{T} \\begin{pmatrix}4\\\\1\\end{pmatrix}\n",
    "\\end{align}\n",
    "\\; \\right\\}\n",
    "\\;\\Leftrightarrow \\;\n",
    "$$\n",
    "</div>\n",
    "<div style = \"float:left;padding-top:0.0cm;\">\n",
    "\n",
    "$\n",
    "\\left\\{ \\; \\begin{align}\n",
    "T \\begin{pmatrix} x \\\\ y \\end{pmatrix} &= T \\left( x \\begin{pmatrix} 1\\\\0\\end{pmatrix} + y \\begin{pmatrix} 0\\\\1\\end{pmatrix} \\right) \\\\\n",
    "                                       &=          x \\begin{pmatrix} 2\\\\3\\end{pmatrix} + y \\begin{pmatrix} 4\\\\1\\end{pmatrix}  \\\\\n",
    "                                       &= \\begin{pmatrix} 2 & 4 \\\\ 3 & 1 \\end{pmatrix} \\begin{pmatrix} x \\\\ y \\end{pmatrix}\n",
    "\\end{align}\n",
    "\\right.\n",
    "$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 1. Application: A Matrix Representation for a Linear Transformation $T: U \\rightarrow V$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"background-color:#F2F5A9;color:black;padding:2mm 2mm 2mm 2mm;\">\n",
    "\n",
    "Let $T : \\;\\; \\tilde{x} \\in U \\xrightarrow{ \\;\\; \\tilde{x} \\ =\\ T x \\;\\;} \\tilde{x} \\in V$ be a transformation<br>\n",
    "$\\quad$ from an $N$ dimensional vector space $U$ over the scalars $\\mathbb{F}$<br>\n",
    "$\\quad$ to\n",
    "an $M$ dimensional vector space $V$ over the scalars $\\mathbb{F}$\n",
    "\n",
    "We will show that when $T$ is a **linear transformation,** it can be **represented by a constant matrix** $\\mathbf{T_A}$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true",
    "tags": []
   },
   "source": [
    "# 2. Finite Dimensional Vector Spaces Can be Represented by $\\mathbb{F}^N$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "Consider a finite dimensional vector space $V$ with a **basis** $\\left\\{ v_1, v_2, \\dots v_n \\right\\}$.\n",
    "\n",
    "Thus, any vector $v \\in V$ can be  **uniquely** represented as a linear combination $v = \\alpha_1 v_1 + \\alpha_2 v_2 \\dots + \\alpha_n v_n.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "Define the following **coordinate vector transformation** $D : V \\rightarrow \\mathbb{F}^n$:\n",
    "* $D$ is a linear transformation\n",
    "* For each basis vector $v_i$, let $D v_i = e_i$, where $e_i$ is the $i^{th}$ standard basis vector in $\\mathbb{F}^n$,<br> i.e., map the $i^{th}$ basis vector $v_i$ onto the $i^{th}$ column of the identity $I_n$.\n",
    "\n",
    "$\\quad$ Thus $D v = D \\left( \\sum_{i=1}^n{\\alpha_i v_i} \\right) = \\sum_{i=1}^n{ \\alpha_i \\ D v_i}\n",
    "= \\begin{pmatrix} \\alpha_1 \\\\ \\alpha_2 \\\\ \\dots \\\\\\alpha_n \\end{pmatrix}, \\quad$ i.e., $D$ maps any vector $v$ to its **coordinate vector**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "**Remarks:**\n",
    "* $D$ is 1-1 and ONTO, and therefore invertible\n",
    "* Since $D$ is a linear transformation, any linear combination of vectors in $V$ can be computed in $\\mathbb{F}^n$\n",
    "* $V$ and $\\mathbb{F}^N$ are said to be **isomorphic**, i.e., to have the same shape."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true",
    "tags": []
   },
   "source": [
    "#### **Example 1**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "<div style=\"float:left;padding-right:1cm;width:40%;\">\n",
    "Let $V = span \\left\\{ 1, x, x^2 \\right\\}$, and let $D: V \\rightarrow \\mathbb{R}^3$<br>\n",
    "$\\quad$ be the coordinate vector transformation defined above. Let\n",
    "\n",
    "$\\quad \\begin{align}\n",
    "& p_1(x) =\\ \\left( 3+2x -4x^2 \\right) \\Leftrightarrow D\\ p_1(x) = \\left( \\begin{array}{rr} 3 \\\\2 \\\\ -4 \\end{array} \\right) \\\\\n",
    "& p_2(x) =\\ \\left( 1-3x +2x^2 \\right) \\Leftrightarrow D\\ p_2(x) = \\left( \\begin{array}{rr} 1 \\\\ -3 \\\\ 2 \\end{array} \\right) \\\\\n",
    "%\n",
    "\\text{ so that } & \\\\\n",
    "& 3 p_1(x) -2 p_2(x) = 7 + 12 x - 16 x^2\n",
    "\\end{align}$\n",
    "</div><div style=\"float:left;padding-left:1cm;border-left:2px solid black;width:40%;\">\n",
    "We can compute this in $\\mathbb{R}^3$ instead:\n",
    "$$\\begin{align}\n",
    "3 p_1(x) - 2 p_2(x) = &\\  D^{-1} D\\ \\left(\\ 3 p_1(x) - 2 p_2(x)\\ \\right) \\\\[2mm]\n",
    "                    = &\\  D^{-1} \\left(\\ 3 D p_1(x) - 2 D p_2(x)\\ \\right) \\\\[2mm]\n",
    "                    = &\\  D^{-1} \\left(\\; 3 \\left( \\begin{array}{rr} 3 \\\\2 \\\\ -4 \\end{array} \\right)\n",
    "                                    - 2 \\left( \\begin{array}{rr} 1 \\\\ -3 \\\\ 2 \\end{array} \\right) \\; \\right) \\\\[2mm]\n",
    "                    = &\\ D^{-1} \\left( \\begin{array}{rr} 7 \\\\ 12 \\\\ -16 \\end{array} \\right) \\\\[2mm]\n",
    "                    = &\\ 7 + 12 x -16 x^2.\n",
    "\\end{align} $$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true",
    "tags": []
   },
   "source": [
    "#### **Example 2: This may require some work**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $V = span\\left\\{ 1, 1+x^2, 1-x^2 \\right\\}$.\n",
    "\n",
    "To use coordinate vectors, we must first **establish that $V$ is a vector space**, and **find a basis** for this vector space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here $V$ is a vector space since it is a span of vectors in $\\mathscr{P}_2(-\\infty,\\infty)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To find a basis for $V$, we can obtain a largest subset of linearly independent vectors from $\\left\\{ p_1(x)=1, p_2(x)=(1+x^2), p_3(x)=(1-x^2) \\right\\}$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-left:1.5cm;width:90%;\">\n",
    "Let $(\\xi) \\Leftrightarrow \\alpha p_1(x) + \\beta p_2(x) + \\gamma p_3(x) = 0, \\quad -\\infty < x < \\infty$\n",
    "    \n",
    "Since this equation for the coefficients $\\alpha, \\beta, \\gamma$ is differentiable,<br>\n",
    "$\\quad$ we can take one (or more) derivatives to obtain<br><br>\n",
    "\n",
    "$\\quad (\\xi) \\Rightarrow \\left\\{ \\begin{align}\n",
    "\\alpha +& \\beta (1+x^2) + \\gamma (1-x^2 ) = \\ & 0 \\\\\n",
    "& 2 x \\beta  - 2 x \\gamma  =& 0 \\\\\n",
    "\\end{align} \\right.$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-left:1.5cm;width:90%;\">\n",
    "There are three unknowns: we will need at least 3 linearly independent equations. Substituting $x = 0,$ $x=1$ and $x = 2$,<br>\n",
    "$\\quad (\\xi) \\Rightarrow \\left\\{ \\begin{align}\n",
    "\\alpha + &\\beta + \\gamma &=&\\ 0 \\\\\n",
    "\\alpha + & 2 \\beta  &=&\\ 0 \\\\\n",
    "&2 \\beta - 2 \\gamma &=&\\ 0 \\\\\n",
    "\\alpha +& 5 \\beta - 3 \\gamma &=&\\ 0,\n",
    "\\end{align} \\right.$<br><br>\n",
    "    \n",
    "where we have omitted obviously redundant equations. This system has solution<br><br>\n",
    "\n",
    "$\\qquad \\begin{pmatrix}\\alpha \\\\ \\beta \\\\ \\gamma \\end{pmatrix} = c \\begin{pmatrix} -2 \\\\1 \\\\1 \\end{pmatrix}\\quad$ for any constant $c$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-left:1.5cm;width:90%;\">\n",
    "We have derived this solution for three values of $x$. We need to check that it holds for all $x$.<br>\n",
    "Substituting in $(\\xi),$ we find that $2 p_1(x) = p_2(x) + p_3(x)$ for all $x$.<br><br>\n",
    "\n",
    "Since we can drop any one of these functions, let us keep $\\mathbf{\\left\\{ p_1(x) = 1, p_2(x) = x^2 + 1 \\right\\}}$, i.e., we chose $\\gamma = 0$ in the equations above.<br><br>\n",
    "Since this choice forces $\\alpha=0, \\beta = 0,$ these two functions are linearly independent, and therefore form **a basis for** $V$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 3. A Linear Transformation $T:U\\rightarrow V$ Represented by $y = A x$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true",
    "tags": []
   },
   "source": [
    "## 3.1 The Method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "<div style=\"float:left;width:40%;\">\n",
    "The relationship between the various spaces is as follows:\n",
    "\n",
    "<img src=\"Figs/LinTxCd.svg\"  width=\"250\">\n",
    "</div>\n",
    "<div style=\"float:left;width:45%;padding-left:1cm;border-left:2px solid black;height:4.6cm;\">\n",
    "The composition of linear transformations is a linear transformation. We therefore have <strong>the following linear transformations</strong>\n",
    "$$\n",
    "\\begin{align}\n",
    "U \\xrightarrow{D_u} \\mathbb{F}^N \\xrightarrow{A_T} \\mathbb{F}^M \\xrightarrow{D_v^{-1}} V \\\\\n",
    "\\mathbb{F}^N \\xrightarrow{D_u^{-1}} U \\xrightarrow{T} V \\xrightarrow{D_v} {\\mathbb{F}^M}\n",
    "\\end{align}\n",
    "$$\n",
    "<br><br>\n",
    "Note $\\quad \\mathbb{F}^N \\xrightarrow{A_T} \\mathbb{F}^M$:\n",
    "$\\quad$ we know how to obtain this matrix!\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "* The path from $\\mathbb{F}^N$ to $\\mathbb{F}^M$ via  $U$ and $V$ is used to determine a matrix representation $A_T$ of size $M \\times N$ of this transformation.\n",
    "* The path from $U$ to $V$ via $\\mathbb{F}^N$ and $\\mathbb{F}^M$ can then be used to compute the transformation $T$ to any vector in $U$ using this matrix."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:** Using this method combines ideas we have been studying:<br>\n",
    "<div style=\"padding-left:1cm;\">\n",
    "\n",
    "Given $T : U \\rightarrow V$\n",
    "* establish both $U$ are $V$ vector spaces (e.g., subspaces of known vector spaces)\n",
    "* establish that $T$ is a linear transformation\n",
    "* pick a basis for $U$ and set up the coordinate vector transformation $D_u$\n",
    "* pick a basis for $V$ and set up the coordinate vector representation $D_v$\n",
    "* obtain the matrix $A_T$\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## 3.2 Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "Consider $\\mathbf{U = \\mathscr{P}_2(\\infty,-\\infty)}$ the vector space of polynomials of degree $\\le 2$,<br>\n",
    "$\\qquad$ and the transformation $\\mathbf{T p(x) = 6 \\int_0^x {p(t)\\ dt} + 3 x \\frac{d}{dx} p(x)}$ with scalars in $\\mathbb{R}$.<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true",
    "tags": []
   },
   "source": [
    "### 3.2.1 Verify $T$ is a linear Transformation From a Vector Space to a Vector Space"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "* A codomain for this transformation is $V = \\mathscr{P}_3(\\infty,-\\infty)$,  the vector space of polynomials of degree $\\le 3$.\n",
    "\n",
    "$\\quad\\quad$Thus $T : \\mathscr{P}_2(\\infty,-\\infty) \\rightarrow \\mathscr{P}_3(\\infty,-\\infty)$, a transformation from a 3-dimensional vector space to a 4-dimensional vector space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "* Is $T$ linear? Let $p_1, p_2$ be any two ploynomials in $U$, and any $\\alpha$ in $\\mathbb{R}$.\n",
    "$$\\begin{align}\n",
    "T (p_1 + p_2) - T p_1 - T p_2 &= \\left( 6 \\int_0^x {\\left(p_1(t)+p_2(t)\\right)\\ dt} + 3 x \\left( (p_1+p_2)' \\right) \\right)\n",
    "                               - \\left( 6 \\int_0^x {p_1(t)\\ dt}                     + 3 x p_1' \\right)\n",
    "                               - \\left( 6 \\int_0^x {p_2(t)\\ dt}                     + 3 x p_2' \\right) &= 0\\\\\n",
    "T (\\alpha p_1 ) - \\alpha T p_1 &=  \\left( 6 \\int_0^x {\\alpha p_1(t)\\ dt}                     + 3 x (\\alpha p_1)' \\right)\n",
    "                                 - \\alpha  \\left( 6 \\int_0^x {p_1(t)\\ dt}                     + 3 x p_1' \\right) &= 0\n",
    "\\end{align}\n",
    "$$\n",
    "<br>\n",
    "Since $T$ is a linear transformation from a 3-dimensional vector space to a 4-dimensional vector space, it can be represented by a matrix."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": "true",
    "tags": []
   },
   "source": [
    "### 3.2.2 Choose Bases and Coordinate Vector Transforms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "* Next, we need to choose bases for $U$ and $V$, and define the coordinate transforms.\n",
    "    * A basis for $\\mathscr{P}_2(\\infty,-\\infty)$ is $\\left\\{ 1, x, x^2 \\right\\}$: $\\quad\\quad$\n",
    "      Let $D_u( \\alpha + \\beta x + \\gamma x^2 ) \\quad\\quad \\;\\ = \\begin{pmatrix} \\alpha & \\beta & \\gamma \\end{pmatrix}^t.$\n",
    "    * A basis for $\\mathscr{P}_3(\\infty,-\\infty)$ is $\\left\\{ 1, x, x^2,x^3 \\right\\}$:$\\quad$\n",
    "      Let $D_v( \\alpha + \\beta x + \\gamma x^2 + \\delta x^3) = \\begin{pmatrix} \\alpha & \\beta & \\gamma & \\delta \\end{pmatrix}^t.$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2.3 Obtain the Matrix Representation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "* We can now compute the matrix $A_T : \\mathbb{R}^3 \\rightarrow \\mathbb{R}^4$\n",
    "$$\\left.\n",
    "\\begin{align}\n",
    "\\begin{pmatrix} 1\\\\0\\\\0 \\end{pmatrix} &\\;\\xrightarrow{D_u^{-1}}\\;\\; p_1(x) = 1   &\\xrightarrow{T}&\\;\\; \\tilde{p_1}(x) = 6 x &\\xrightarrow{D_v}\\; \\begin{pmatrix}0\\\\6\\\\0\\\\0\\end{pmatrix}\\\\\n",
    "\\begin{pmatrix} 0\\\\1\\\\0 \\end{pmatrix} &\\;\\xrightarrow{D_u^{-1}}\\;\\; p_2(x) = x   &\\xrightarrow{T}&\\;\\; \\tilde{p_1}(x) = 3 x^2+3x &\\xrightarrow{D_v}\\; \\begin{pmatrix}0\\\\3\\\\3\\\\0\\end{pmatrix}\\\\\n",
    "\\begin{pmatrix} 0\\\\0\\\\1 \\end{pmatrix} &\\;\\xrightarrow{D_u^{-1}}\\;\\; p_3(x) = x^2 &\\xrightarrow{T}&\\;\\; \\tilde{p_1}(x) = 2 x^3+6x^2 &\\xrightarrow{D_v}\\; \\begin{pmatrix}0\\\\0\\\\6\\\\2\\end{pmatrix}\\\\\n",
    "\\end{align}\\qquad\n",
    "\\right\\} \\quad \\Rightarrow \\quad\n",
    "A_T = \\begin{pmatrix}0&0&0 \\\\ 6&3&0\\\\ 0&3&6 \\\\ 0&0&2 \\end{pmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "**Remarks:**\n",
    "* The transformation is therefore 1-1, but not ONTO: $\\quad T p(x) = \\alpha \\quad$ does not have a solution for any $\\alpha \\in \\mathbb{R}.$\n",
    "* We could have chosen the codomain $V = span \\left\\{ x, x^2, x^3 \\right\\}$, in which case the transformation would have been invertible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "**Additional Remark:** A non-linear transformation defined for polynomials:<br>\n",
    "$\\quad\\quad$Consider instead the transformation  $T p(x) = \\int_0^x {\\left(1+p(t)\\right)\\ dt} + 3 x \\frac{d}{dx} p(x)$.\n",
    "<br>\n",
    "$\\quad\\quad$Let $p_1, p_2$ be any two ploynomials in $U$. We find\n",
    "$$\n",
    "T (p_1 + p_2) - T p_1 - T p_2 = \\left( \\int_0^x {(1+p_1+p_2) dt}+ 3 x (p_1+p_2)' \\right) -\\left( \\int_0^x {(1+p_1)dt} + 3 x p_1' \\right)-\\left( \\int_0^x {(1+p_2)dt} + 3 x p_2' \\right) = - x.\n",
    "$$\n",
    "<br>\n",
    "$\\quad\\quad$Since this transformation is not linear on $U,$ it **does not have a matrix representation**<br>\n",
    "$\\qquad\\quad$<strong> even though we could assemble a matrix in the way described above</strong>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### 3.2.4 Using the Matrix for Computations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "$\n",
    "T ( 5 + 2 x + 3 x^2 ) =  D^{-1}_v    A_T \\begin{pmatrix} 5\\\\2\\\\3 \\end{pmatrix} =  D^{-1}_v \\ \\begin{pmatrix}0&0&0 \\\\ 6&3&0\\\\ 0&3&6 \\\\ 0&0&2 \\end{pmatrix} \\begin{pmatrix} 5\\\\2\\\\3 \\end{pmatrix}  = D^{-1}_v \\ \\begin{pmatrix} 0 \\\\ 36 \\\\ 24 \\\\ 6 \\end{pmatrix} = 36 x + 24 x^2 + 6 x^3 $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### 3.2.5 Implementation Using SymPy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true
    },
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div><p style=\"color:blue;font-size:15pt;height:15px;text-align:left;\">Compute T using Sympy:</p></div>"
      ],
      "text/plain": [
       "HTML{String}(\"<div><p style=\\\"color:blue;font-size:15pt;height:15px;text-align:left;\\\">Compute T using Sympy:</p></div>\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\t ->\t 6*x\n",
      "x\t ->\t 3*x^2 + 3*x\n",
      "x^2\t ->\t 2*x^3 + 6*x^2\n"
     ]
    }
   ],
   "source": [
    "LAcodes.title(\"Compute T using Sympy:\", sz=15, height=15)\n",
    "using SymPy, Latexify\n",
    "x=symbols(\"x\")\n",
    "\n",
    "function t(p)\n",
    "    p̃ = 6*integrate(p, x)+3*x*diff(p,x)\n",
    "    expand(p̃)\n",
    "end\n",
    "for p in [x^0, x^1, x^2]\n",
    "    p̃ = t(p)\n",
    "    println( p,\"\\t ->\\t $p̃\")\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "source": [
    "We now use the linear transformation we have defined."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true,
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div><p style=\"color:blue;font-size:15pt;height:15px;text-align:left;\">Set up coefficient vector transformations (these work for both $D_u$ and $D_v)$</p></div>"
      ],
      "text/plain": [
       "HTML{String}(\"<div><p style=\\\"color:blue;font-size:15pt;height:15px;text-align:left;\\\">Set up coefficient vector transformations (these work for both \\$D_u\\$ and \\$D_v)\\$</p></div>\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "LAcodes.title(\"Set up coefficient vector transformations (these work for both \"*L\"D_u\"*\" and \"*L\"D_v)\", sz=15,height=15)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "function D(p, d=nothing)\n",
    "    if d == nothing\n",
    "        return N.([p(x=>0); [p.coeff(x^i) for i in 1:N(degree(p,gen=x))] ])\n",
    "    end\n",
    "    N.([p(x=>0); [p.coeff(x^i) for i in 1:d] ])\n",
    "end\n",
    "function Dinv(v)\n",
    "    p = 0\n",
    "    for (i,c) in enumerate(v)\n",
    "        p += c*x^(i-1)\n",
    "    end\n",
    "    p\n",
    "end\n",
    "if false\n",
    "    p = x+3*x^2\n",
    "    println( \"Test for $p\" )\n",
    "    println( \".  D(p)   = $(D(p))\")\n",
    "    println( \".  Dinv(D(p)) - p  = $(Dinv(D(p))-p)\")\n",
    "    D(Dinv(D(p)))'\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true,
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div><p style=\"color:blue;font-size:15pt;height:15px;text-align:left;\">Define $A_T$</p></div>"
      ],
      "text/plain": [
       "HTML{String}(\"<div><p style=\\\"color:blue;font-size:15pt;height:15px;text-align:left;\\\">Define \\$A_T\\$</p></div>\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "4×3 Array{Int64,2}:\n",
       " 0  0  0\n",
       " 6  3  0\n",
       " 0  3  6\n",
       " 0  0  2"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "LAcodes.title(\"Define \" * L\"A_T\", sz=15,height=15)\n",
    "A_T = [0 0 0; 6 3 0; 0 3 6; 0 0 2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true,
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div><p style=\"color:blue;font-size:15pt;height:15px;text-align:left;\">Use it for a polynomial</p></div>"
      ],
      "text/plain": [
       "HTML{String}(\"<div><p style=\\\"color:blue;font-size:15pt;height:15px;text-align:left;\\\">Use it for a polynomial</p></div>\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/latex": [
       "$\\begin{equation*}3 x^{2} + 2 x + 1\\end{equation*}$\n"
      ],
      "text/plain": [
       "   2          \n",
       "3*x  + 2*x + 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Using T directly, we get:  6*x^3 + 24*x^2 + 12*x\n",
      "Using A, we get:           6*x^3 + 24*x^2 + 12*x\n"
     ]
    }
   ],
   "source": [
    "LAcodes.title( \"Use it for a polynomial\", sz=15, height=15)\n",
    "function useA(p) p_u = D(p); p̃_v = A_T * p_u; p̃ = Dinv( p̃_v ); p̃ end\n",
    "    \n",
    "p = 1 + 2x + 3x^2\n",
    "display(p)\n",
    "println(\"Using T directly, we get:  \", t(p) )\n",
    "println(\"Using A, we get:           $(useA(p))\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": true,
    "jupyter": {
     "outputs_hidden": true,
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div><p style=\"color:blue;font-size:15pt;height:15px;text-align:left;\">Automate $A_T$ computation</p></div>"
      ],
      "text/plain": [
       "HTML{String}(\"<div><p style=\\\"color:blue;font-size:15pt;height:15px;text-align:left;\\\">Automate \\$A_T\\$ computation</p></div>\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "4×3 Array{Int64,2}:\n",
       " 0  0  0\n",
       " 6  3  0\n",
       " 0  3  6\n",
       " 0  0  2"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "LAcodes.title(\"Automate \"*L\"A_T\"*\" computation\", sz=15, height=15)\n",
    "\n",
    "# Compute the matrix representation A_T of the transformation T\n",
    "function get_A_T( t, dim_U, Du, Du_inv, dim_V, Dv, Dv_inv )\n",
    "    A = Array{Float64}( undef, dim_V, dim_U )\n",
    "\n",
    "    for i in 1:dim_U\n",
    "        v = zeros(Float64, dim_V); v[i] = 1.\n",
    "        p = Du_inv( v )\n",
    "        p̃ = t(p)\n",
    "        A[:,i] = Dv( p̃ )\n",
    "    end\n",
    "    A\n",
    "end\n",
    "\n",
    "A_T = Int64.(get_A_T( t, 3, x->D(x,4), Dinv, 4, x->D(x,3), Dinv))\n",
    "A_T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "##### Use $\\mathbb{F}^n$ rather than $U$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "We can now use the matrix representation to do computations with matrix library code: consider\n",
    "$$\n",
    "\\tilde{p} \\xrightarrow{D_v^{-1}} v \\xrightarrow{ u = A_T v } u \\xrightarrow{D_u^{-1}} p\n",
    "$$\n",
    "\n",
    "Given a polynomial $\\tilde{p}(x) \\in V$, this computation yields a polynomial $p(x) \\in U$<br>\n",
    "$\\quad$ such that $T p(x) = \\tilde{p}(x).$\n",
    "\n",
    "To find all possible preimages, we need to include the homogeneous solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div><p style=\"color:blue;font-size:15pt;height:15px;text-align:left;\">A preimage of T</p></div>"
      ],
      "text/plain": [
       "HTML{String}(\"<div><p style=\\\"color:blue;font-size:15pt;height:15px;text-align:left;\\\">A preimage of T</p></div>\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Preimage( 2*x^3 - 3*x^2 + 6*x )  =  1.0*x^2 - 3.0*x + 2.5\n",
      "The preimage is unique since T is 1-1:  true\n"
     ]
    }
   ],
   "source": [
    "LAcodes.title( \"A preimage of T\", sz=15, height=15)\n",
    "p̃ = 2x^3 - 3x^2 + 6x\n",
    "v = D(p̃, 3)\n",
    "u = A_T \\ v\n",
    "p = Dinv(u)\n",
    "println( \"Preimage( $p̃ )  =  $p\" )\n",
    "\n",
    "println( \"The preimage is unique since T is 1-1:  $(isempty(nullspace(A_T)))\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div><p style=\"color:blue;font-size:15pt;height:15px;text-align:left;\">check the error</p></div>"
      ],
      "text/plain": [
       "HTML{String}(\"<div><p style=\\\"color:blue;font-size:15pt;height:15px;text-align:left;\\\">check the error</p></div>\")"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/latex": [
       "$\\begin{equation*}4.44089209850063 \\cdot 10^{-16} x^{3} + 1.77635683940025 \\cdot 10^{-15} x\\end{equation*}$\n"
      ],
      "text/plain": [
       "                      3                         \n",
       "4.44089209850063e-16*x  + 1.77635683940025e-15*x"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "LAcodes.title(\"check the error\",sz=15)\n",
    "t(p)-p̃"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# 4. Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "* A finite n-dimensional vector space can be represented by $\\mathbb{F}^n$\n",
    "    * We can perform computations with linear combinations in either the original vector space<br>\n",
    "      or equivalently in $\\mathbb{F}^n$\n",
    "    * linear transformations on vectors in a vector space can be represented by a matrix acting on the vectors in $\\mathbb{F}^n$.\n",
    "    * We can use linear algebra techniques to answer questions such as existence and uniqueness of solutions"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.4",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
